import {Actions} from 'react-native-router-flux'
import {
    PRODUCT_DATA,
    CREATE_PRODUCT,
    CREATE_PRODUCT_DATA_FAIL,
    CREATE_PRODUCT_DATA_SUCCESS,
    EDIT_PRODUCT_DATA_SUCCESS,
    EDIT_PRODUCT_DATA_FAIL,
    ID_CHARITIE,
    DATA_SEARCHED,
    DATA_ACTIVE_PRODUCT,
    DATA_INACTIVE_PRODUCT,
    ACTIVE_EDIT_PRODUCTS,
    SET_DELETE_PRODUCTS,
    SELECT_WINNER
} from './types'
import Axios from 'axios';
export const setProductDelete=(value)=>{
    return{
        type: SET_DELETE_PRODUCTS,
        payload: value
    }
}
export const editProductDelete=(value)=>{
    return{
        type: ACTIVE_EDIT_PRODUCTS,
        payload: value
    }
}

export const activeProductData=()=>{
    return(dispatch) =>{
        Axios({
            method: 'GET',
            url: 'https://donate2win.herokuapp.com/products/current',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                Authorization: "bearer "+"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkZPUlRBU1RFQVVBIiwiaWF0IjoxNTE2MjM5MDIyfQ.jk9hAMZ6iy2qdPUKJWdviqwNdvQW017PMQr1NUae8VE"

            },
        }).then(json => {
            dispatch({
                type: DATA_ACTIVE_PRODUCT, 
                payload: json.data.products})
        })
    }
}
export const inactiveProductData=()=>{
    return(dispatch) =>{
        Axios({
            method: 'GET',
            url: 'https://donate2win.herokuapp.com/products/expired',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                Authorization: "bearer "+"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkZPUlRBU1RFQVVBIiwiaWF0IjoxNTE2MjM5MDIyfQ.jk9hAMZ6iy2qdPUKJWdviqwNdvQW017PMQr1NUae8VE"

            },
        }).then(json => {
            dispatch({
                type: DATA_INACTIVE_PRODUCT, 
                payload: json.data.products})
        })
    }
}
export const idChanged=(text)=>{
    return{
        type: ID_CHARITIE,
        payload: text
    }
}
export const productSearched=(text)=>{
    return{
        type: DATA_SEARCHED,
        payload: text
    }
}
export const productsData = () =>{
    return(dispatch) =>{
        Axios({
            method: 'GET',
            url: 'https://donate2win.herokuapp.com/products',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                Authorization: "bearer "+"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkZPUlRBU1RFQVVBIiwiaWF0IjoxNTE2MjM5MDIyfQ.jk9hAMZ6iy2qdPUKJWdviqwNdvQW017PMQr1NUae8VE"

            },
        }).then(json => {
            dispatch({
                type: PRODUCT_DATA, 
                payload: json.data.products})
        })
    }
}
export const deleteProduct=({token},_id)=>{
    return(dispatch)=>{
        dispatch({type: CREATE_PRODUCT})
        Axios({
            method: 'DELETE',
            url: `https://donate2win.herokuapp.com/products/${_id}`,
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token
            },
        }).then(()=>{
            Actions.postMain()
        })
    }
}
export const editProduct=({name,img_url,description,price,charity},token,_id)=>{
    return(dispatch)=>{
        dispatch({type: CREATE_PRODUCT})
        Axios({
            method: 'PUT',
            url: `https://donate2win.herokuapp.com/products/${_id}`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token 
            },
            data:{
                "name": name,
                "img_url": img_url,
                "description": description,
                "price": price,
                "charity": charity
            },
        })
        .then(response=>{
            if(response.data.error)
                editProductFail(dispatch,true)
            else
                editProductSuccess(dispatch)
        }).catch(error =>{
            editProductFail(dispatch,true)
        })
    }
}
export const createProduct=({name,img_url,description,price,charity},token)=>{
    return (dispatch)=>{
        
        dispatch({type: CREATE_PRODUCT})
        Axios({
            method: 'POST',
            url: 'https://donate2win.herokuapp.com/products',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token 
            },
            data:{
                "name": name,
                "img_url": img_url,
                "description": description,
                "price": price,
                "charity": charity,
            },
        })
       .then(response=>{
           if(response.data.error)
                createProductFail(dispatch,true)
            else
                createProductSuccess(dispatch)    
        })
        .catch(error => {
            createProductFail(dispatch,true)
        })
    }
}
export const createProductFail=(dispatch,value)=>{
    dispatch({
        type: CREATE_PRODUCT_DATA_FAIL,
        payload: value
})
}
export const createProductSuccess=(dispatch)=>{
    dispatch({type: CREATE_PRODUCT_DATA_SUCCESS})
    Actions.postMain()
}
export const editProductSuccess=(dispatch)=>{
    dispatch({type: EDIT_PRODUCT_DATA_SUCCESS})
    Actions.postMain()
}
export const editProductFail=(dispatch,value)=>{
    dispatch({
        type: EDIT_PRODUCT_DATA_FAIL,
        payload: value
    })
}
export const productErrorEdit=(value)=>{
    return({
        type: EDIT_PRODUCT_DATA_FAIL,
        payload: value
    })
}
export const productErrorCreate=(value)=>{
    return({
        type: CREATE_PRODUCT_DATA_FAIL,
        payload: value
    })
}
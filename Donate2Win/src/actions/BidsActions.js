import {
    BID_DATA,
    BID_DATA_RESET
} from './types'
import Axios from "axios";
export const resetData=()=>{
    return{
        type: BID_DATA_RESET,
    }
}
export const bidData = ({token},_id) =>{
    return(dispatch) =>{
        Axios({
            method: 'GET',
            url: ` https://donate2win.herokuapp.com/bids/user/${_id}`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token
            },
        }).then(json => {
            dispatch({
                type: BID_DATA, 
                payload: json.data.bids
                
            })
        })
    }
}

import {LOGIN_ADMIN,LOGIN_SUCCESS,LOGIN_FAIL} from './types'
import  {Actions} from 'react-native-router-flux'
import Axios from 'axios';


export const loginAdmin=({email,password}) =>{
    return(dispatch)=>{
        dispatch({type: LOGIN_ADMIN})
        Axios({
            method: 'POST',
            url: 'https://donate2win.herokuapp.com/auth/login',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                Authorization: "bearer "+"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkZPUlRBU1RFQVVBIiwiaWF0IjoxNTE2MjM5MDIyfQ.jk9hAMZ6iy2qdPUKJWdviqwNdvQW017PMQr1NUae8VE"
            },
            data:{
                "email": email,
                "password": password,
            },
        }).then(token=> { 
            if(token.data.error || token.data.user.role=="user")
                loginFail(dispatch,true)
            else
                loginSuccess(dispatch,token.data) 
          }).catch(() => {loginFail(dispatch,true)})
    }
}
export const loginFail=(dispatch,value)=>{
    dispatch({
        type: LOGIN_FAIL,
        payload: value
    })
}
export const loginSuccess=(dispatch,token)=>{
    dispatch({
        type: LOGIN_SUCCESS,
        payload: token
    })
    Actions.postMain()
}
export const isError=(value)=>{
    return({
        type: LOGIN_FAIL,
        payload: value
    })
}
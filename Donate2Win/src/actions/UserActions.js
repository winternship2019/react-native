import { Actions } from "react-native-router-flux";
import RNFetchBlob from 'rn-fetch-blob'
import {
    CREATE_ACCOUNT,
    CREATE_USER_FAIL,
    CREATE_USER_SUCCESS,
    EDIT_USER_FAIL,
    EDIT_USER_SUCCESS,
    USERS_DATA,
    DATA_USER_SEARCHED
} from './types'
import Axios from "axios";
export const userSearched=(text)=>{
    return{
        type: DATA_USER_SEARCHED,
        payload: text
    }
}
export const usersData = ({token}) =>{
    return(dispatch) =>{
        Axios({
            method: 'GET',
            url: 'https://donate2win.herokuapp.com/users',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token
            },
        })
        .then(json => {
            dispatch({
                type: USERS_DATA, 
                payload: json.data.users})
        })
    }
}
export const editUser=({token},_id,{name,email,img_url,role,wallet,password})=>{
    return(dispatch)=>{
        dispatch({type: CREATE_ACCOUNT})
        RNFetchBlob.fetch('POST',
            `https://donate2win.herokuapp.com/users/${_id}`,
            {
                'Content-type':'multipart/form-data',
                Authorization: "bearer "+token 
            },[
                {name:'email', data: email},
                {name:'name', data: name},
                {name:'role', data: role},
                {name:'wallet', data: `${wallet}`},
                {name:'password', data: password},
                {name:'img_url', filename : img_url.fileName, type: img_url.type, data: RNFetchBlob.wrap(img_url.path)},
            ]
        )
        .then(()=>{
            editUserSuccess(dispatch)
        })
    }
}
export const deleteUser=({token},_id)=>{
    return(dispatch)=>{
        dispatch({type: CREATE_ACCOUNT})
        Axios({
            method: 'DELETE',
            url: `https://donate2win.herokuapp.com/users/${_id}`,
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token
            },
        })
        .then(()=>{
            usersData(token)
            Actions.userMain()
        })
    }
}
export const createAccount=({name,email,password,role,wallet,userImage},token)=>{
    return (dispatch)=>{
        dispatch({type: CREATE_ACCOUNT})
        RNFetchBlob.fetch('POST',
            'https://donate2win.herokuapp.com/users',
            {
                'Content-type':'multipart/form-data',
                Authorization: "bearer "+token 
            },[
                {name:'email', data: email},
                {name:'name', data: name},
                {name:'password', data: password},
                {name:'wallet', data: wallet},
                {name:'role', data: role},
                {name:'img_url', filename : userImage.fileName, type: userImage.type, data: RNFetchBlob.wrap(userImage.path)}
            ]
        )
        .then(()=>{
                createSuccess(dispatch)    
        
            })
    }
}
export const createFail=(dispatch)=>{
    dispatch({type: CREATE_USER_FAIL})
}
export const createSuccess=(dispatch)=>{
    dispatch({type: CREATE_USER_SUCCESS})
    Actions.userMain()
}
export const editUserSuccess=(dispatch)=>{
    dispatch({type: EDIT_USER_SUCCESS})
    Actions.userMain()
}
export const editUserFail=(dispatch)=>{
    dispatch({type: EDIT_USER_FAIL})
}
export const LOGIN_ADMIN='login_admin'
export const LOGIN_FAIL='login_fail'
export const LOGIN_SUCCESS='login_success'
export const LOGOUT_ADMIN='logout_admin'

export const USERS_DATA='users_data'
export const CREATE_ACCOUNT='create_account'
export const CREATE_USER_FAIL='create_user_fail'
export const CREATE_USER_SUCCESS='create_user_success'
export const EDIT_USER_SUCCESS='edit_user_success'
export const EDIT_USER_FAIL='edit_user_fail'
export const DATA_USER_SEARCHED='data_user_searched'

export const CHARITIE_DATA='charitie_data'
export const CREATE_CHARITIE='create_charitie'
export const CREATE_CHARITIE_FAIL='create_charitie_fail'
export const CREATE_CHARITIE_SUCCESS='create_charitie_success'
export const EDIT_CHARITIE_SUCCESS='edit_charitie_success'
export const EDIT_CHARITIE_FAIL='edit_charitie_fail'
export const DATA_CHARITIE_SEARCHED="data_charitie_searched"

export const EDIT_ADMIN_SUCCESS='edit_admin_success'
export const EDIT_ADMIN_FAIL='edit_admin_fail'
export const ADMIN_ACCOUNT='admin_account'
export const PASSWORD_RESET='password_reset'
export const ADMIN_DATA='admin_data'

export const PRODUCT_DATA='product_data'
export const CREATE_PRODUCT='create_product'
export const CREATE_PRODUCT_DATA_FAIL='create_product_fail'
export const CREATE_PRODUCT_DATA_SUCCESS='create_product_success'
export const EDIT_PRODUCT_DATA_SUCCESS='edit_product_success'
export const EDIT_PRODUCT_DATA_FAIL='edit_products_fail'
export const ID_CHARITIE='id_charitie'
export const DATA_SEARCHED="data_searched"
export const DATA_ACTIVE_PRODUCT="data_active_product"
export const DATA_INACTIVE_PRODUCT="data_inactive_product"
export const ACTIVE_EDIT_PRODUCTS='active_edit_products'
export const SET_DELETE_PRODUCTS='set_delete_products'
export const SELECT_WINNER='select_winner'

export const BID_DATA="bid_data"
export const BID_DATA_RESET="bid_data_reset"
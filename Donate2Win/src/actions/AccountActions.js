import { Actions } from "react-native-router-flux";
import {Alert} from 'react-native'
import RNFetchBlob from 'rn-fetch-blob'
import {
    EDIT_ADMIN_SUCCESS,
    EDIT_ADMIN_FAIL,
    ADMIN_ACCOUNT,
    PASSWORD_RESET,
    ADMIN_DATA,
    SELECT_WINNER
} from './types'
import Axios from "axios";
export const adminData = (token,_id) =>{
    return(dispatch) =>{
        Axios({
            method: 'GET',
            url: `https://donate2win.herokuapp.com/users/${_id}`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token
            },
        }).then(json => {
            dispatch({
                type: ADMIN_DATA, 
                payload: json.data.users
            })
        })
    }
}
export const editAdmin=(token,_id,{name,email,img_url,wallet,password,passwordO})=>{
    return(dispatch)=>{
        dispatch({type: ADMIN_ACCOUNT})
        RNFetchBlob.fetch('POST',
        `https://donate2win.herokuapp.com/users/${_id}`,
        {
            'Content-type':'multipart/form-data',
            Authorization: "bearer "+token 
        },[
            {name:'email', data: email},
            {name:'name', data: name},
            {name:'role', data: 'admin'},
            {name:'wallet', data: `${wallet}`},
            {name:'password', data: password},
            {name:'old_password', data: passwordO},
            {name:'img_url', filename : img_url.fileName, type: img_url.type, data: RNFetchBlob.wrap(img_url.path)}
        ]
    )
    .then((res)=>{
        editAdminSuccess(dispatch)
    })
}
}
export const resetPassword=(email)=>{
    return(dispatch)=>{
        dispatch({type: ADMIN_ACCOUNT})
        Axios({
            method: 'POST',
            url: 'https://donate2win.herokuapp.com/reset/password',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                Authorization: "bearer "+"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkZPUlRBU1RFQVVBIiwiaWF0IjoxNTE2MjM5MDIyfQ.jk9hAMZ6iy2qdPUKJWdviqwNdvQW017PMQr1NUae8VE"
            },
            data:{
                "email": email
            }
        }).then((res)=>{
            
            if(res.data.error)
                emailSentFail(dispatch,true)
            else
                emailSentSuccess(dispatch)
        }).catch((error)=>{
            emailSentFail(dispatch,true)
        })
        
    }
}
export const selectWinner=(token)=>{
    return (dispatch)=>{
        
        dispatch({type: SELECT_WINNER})
        Axios({
            method: 'POST',
            url: 'https://donate2win.herokuapp.com/bids/selectwinner',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+ token 
            },
            data:{},
        })
       .then(response=>{
            if(response.data.message)
                return(
                    Alert.alert(
                        "",
                        response.data.message,
                        [
                            {text:'Ok'},
                        ],
                        {cancelable: true},
                    )
                )
        })
        .catch(error => {
            
        })
    }
}
export const editAdminSuccess=(dispatch)=>{
    dispatch({type: EDIT_ADMIN_SUCCESS})
    Actions.accountMain()
}
export const editAdminFail=(dispatch)=>{
    dispatch({type: EDIT_ADMIN_FAIL})
}
export const emailSentSuccess=(dispatch)=>{
    dispatch({type: PASSWORD_RESET})
    Actions.auth()
}
export const emailSentFail=(dispatch,value)=>{
    dispatch({
        type: EDIT_ADMIN_FAIL,
        payload: value
    })
}
export const resetError=(value)=>{
    return({
        type: EDIT_ADMIN_FAIL,
        payload: value
    })
}
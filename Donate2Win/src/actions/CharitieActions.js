import {Actions} from 'react-native-router-flux'
import {
    CHARITIE_DATA,
    CREATE_CHARITIE,
    CREATE_CHARITIE_FAIL,
    CREATE_CHARITIE_SUCCESS,
    EDIT_CHARITIE_SUCCESS,
    EDIT_CHARITIE_FAIL,
    DATA_CHARITIE_SEARCHED
} from './types'
import Axios from 'axios';
export const charitieSearched=(text)=>{
    return{
        type: DATA_CHARITIE_SEARCHED,
        payload: text
    }
}
export const charitiesData = () =>{
    return(dispatch) =>{
        Axios({
            method: 'GET',
            url: 'https://donate2win.herokuapp.com/charities',
            headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: "bearer "+"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkZPUlRBU1RFQVVBIiwiaWF0IjoxNTE2MjM5MDIyfQ.jk9hAMZ6iy2qdPUKJWdviqwNdvQW017PMQr1NUae8VE"
            },
        })
        .then(json => {
            dispatch({
                type: CHARITIE_DATA, 
                payload: json.data.charities})
        })
        
    }
}
export const editCharitie=({token},_id,{name,description,img_url,phone_number,website})=>{
    return(dispatch)=>{
        console.log(img_url)
        dispatch({type: CREATE_CHARITIE})
        Axios({
            method: 'PUT',
            url: `https://donate2win.herokuapp.com/charities/${_id}`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token    
            },
            data:{
                "name": name,
                "description": description,
                "phone_number":phone_number,
                "website":website,
                "img_url": img_url,
            },
        })
        .then(response=>{
            editCharitieSuccess(dispatch)
        }).catch(error=>{
            editCharitieFail(dispatch)
        })
    }
}
export const deleteCharitie=({token},_id)=>{
    return()=>{
        Axios({
            method: 'DELETE',
            url: `https://donate2win.herokuapp.com/charities/${_id}`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token   
            },
        })
        .then(()=>{
            charitiesData(token)
            Actions.charitieMain()
        })
    }
}
export const createCharitie = ({name,description,image,phone_number,website},{token}) =>{
    return(dispatch)=>{
        
        dispatch({type: CREATE_CHARITIE})
        Axios({
            method: 'POST',
            url: 'https://donate2win.herokuapp.com/charities',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+token    
            },
            data:{
                "name": name,
                "description": description,
                "img_url": image,
                "phone_number":phone_number,
                "website":website,
            },
        })
        .then(response=>{
            createCharitieSuccess(dispatch)
        }).catch(error => {
            createCharitieFail(dispatch)
        })
        
    }
}
export const createCharitieFail=(dispatch)=>{
    dispatch({type: CREATE_CHARITIE_FAIL})
}
export const createCharitieSuccess=(dispatch)=>{
    dispatch({type: CREATE_CHARITIE_SUCCESS})
    Actions.charitieMain()
}
export const editCharitieSuccess=(dispatch)=>{
    dispatch({type: EDIT_CHARITIE_SUCCESS})
    Actions.charitieMain()
}
export const editCharitieFail=(dispatch)=>{
    dispatch({type: EDIT_CHARITIE_FAIL})
}
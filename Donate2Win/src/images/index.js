const images ={
    background: require('./bg-pattern-grey-full.png'),
    backgroundProducts: require('./bg-pattern-grey-short.png'),
    splash: require('./Logo-positive.png'),
    searchIcon: require('./Search_icon.png'),

    productActivIcon: require('./tab-bar-icon-products-active.png'),
    productIcon: require('./tab-bar-icon-products.png'),

    userIcon: require('./tab-bar-icon-users.png'),
    userActivIcon: require('./tab-bar-icon-users-active.png'),

    charityIcon: require('./tab-bar-icon-causes.png'),
    charityActivIcon: require('./tab-bar-icon-causes-active.png'),

    profileIcon: require('./tab-bar-icon-account.png'),
    profileActivIcon: require('./tab-bar-icon-account-active.png'),

    backPinkArrow: require('./Back-pink.png'),
    backWhiteArrow: require('./Back_white.png'),

    addProductIcon: require('./icon-add.png'),
}
export default images
import {
    ADMIN_EDIT,
    LOGOUT_ADMIN,
    LOGIN_ADMIN, 
    LOGIN_SUCCESS, 
    LOGIN_FAIL
} from '../actions/types'
const INITAL_STATE={token: '', user: '',error: '', loading: false}

export default (state = INITAL_STATE, action)=>{
    switch(action.type){
        case LOGIN_ADMIN:
            return {...state, loading: true, error: false}
        case LOGIN_SUCCESS:
            return {
                ...state,
                ...INITAL_STATE,
                token: action.payload.token,
                user: action.payload.user
            }
        case ADMIN_EDIT:
            return{
                ...state,
                user:action.payload
            }
        case LOGIN_FAIL:
            return{ ...state,loading: false, error:action.payload}
        case LOGOUT_ADMIN:
            return{...state,...INITAL_STATE}
        default:
            return state
    }
}
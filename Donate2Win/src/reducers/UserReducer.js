import {
    CREATE_ACCOUNT,
    CREATE_USER_FAIL,
    CREATE_USER_SUCCESS,
    EDIT_USER_FAIL,
    EDIT_USER_SUCCESS,
    USERS_DATA,
    DATA_USER_SEARCHED
} from '../actions/types'
const INITAL_STATE={
    loading: false,
    error: '',
    data:'',
    dataCopy:''
}

export default(state = INITAL_STATE, action)=>{
    switch(action.type){
        case USERS_DATA:
            return  {...state, data:action.payload, dataCopy:action.payload}
        case CREATE_ACCOUNT:
            return {...state, loading: true, error: ''}
        case CREATE_USER_SUCCESS:
            return  {...state, ...INITAL_STATE}
        case CREATE_USER_FAIL:
            return  {...state, loading: false,error: 'Nu merge'}
        case EDIT_USER_FAIL:
            return  {...state, loading: false,error: 'Nu merge'}
        case EDIT_USER_SUCCESS:
            return  {...state, ...INITAL_STATE}
        case DATA_USER_SEARCHED:
            return {...state, dataCopy: action.payload}
        default:
            return state
    }
}
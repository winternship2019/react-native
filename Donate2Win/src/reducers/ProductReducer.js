import {
    PRODUCT_DATA,
    CREATE_PRODUCT,
    CREATE_PRODUCT_SUCCESS,
    CREATE_PRODUCT_DATA_FAIL,
    EDIT_PRODUCT_DATA_FAIL,
    EDIT_PRODUCT_DATA_SUCCESS,
    ID_CHARITIE,
    DATA_SEARCHED,
    DATA_ACTIVE_PRODUCT,
    DATA_INACTIVE_PRODUCT,
    ACTIVE_EDIT_PRODUCTS,
    SET_DELETE_PRODUCTS,
} from '../actions/types'
const INITAL_STATE={
    loading: false,
    error: false,
    charitieId:'',
    data: '',
    dataCopy:'',
    activeProduct:'',
    inactiveProduct:'',
    editActive: false,
    deleteProducts:[]
}

export default (state = INITAL_STATE, action)=>{
    switch(action.type){
        case PRODUCT_DATA:
            return  {...state, data: action.payload, dataCopy: action.payload}            
        case CREATE_PRODUCT:
            return {...state, loading: true, error: false}
        case DATA_ACTIVE_PRODUCT:
            return {...state, activeProduct:action.payload}
        case DATA_INACTIVE_PRODUCT:
            return {...state, inactiveProduct:action.payload}
        case CREATE_PRODUCT_SUCCESS:
            return  {...state, ...INITAL_STATE}
        case ACTIVE_EDIT_PRODUCTS:
            return {...state, editActive:action.payload}
        case CREATE_PRODUCT_DATA_FAIL:
            return  {...state, loading: false, error: action.payload}
        case EDIT_PRODUCT_DATA_FAIL:
            return  {...state, loading: false, error: action.payload}
        case EDIT_PRODUCT_DATA_SUCCESS:
            return  {...state, ...INITAL_STATE}
        case ID_CHARITIE:
            return {...state, charitieId: action.payload}
        case DATA_SEARCHED:
            return {...state, dataCopy: action.payload}
        case SET_DELETE_PRODUCTS:
            return {...state, deleteProducts: action.payload}
        default:
            return state
    }
}
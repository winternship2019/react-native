import {
    EDIT_ADMIN_SUCCESS,
    EDIT_ADMIN_FAIL,
    ADMIN_ACCOUNT,
    PASSWORD_RESET,
    ADMIN_DATA,
    SELECT_WINNER
} from '../actions/types'
const INITAL_STATE={
    loading: false,
    error: false,
    users:'',
}

export default(state = INITAL_STATE, action)=>{
    switch(action.type){
        case SELECT_WINNER:
            return  {...state}
        case ADMIN_DATA:
            return  {...state, users:action.payload}            
        case ADMIN_ACCOUNT:
            return {...state, loading: true, error: false,mail: ''}
        case EDIT_ADMIN_FAIL:
            return  {...state, loading: false,error: action.payload}
        case EDIT_ADMIN_SUCCESS:
            return  {...state, ...INITAL_STATE}
        case PASSWORD_RESET:
            return  {...state, loading:false, error:false}
        default:
            return state
    }
}
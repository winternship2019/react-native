import {
    BID_DATA,
    BID_DATA_RESET
} from '../actions/types'
const INITAL_STATE={
    data:'',
}
export default (state = INITAL_STATE, action)=>{
    switch(action.type){
        case BID_DATA:
            return {...state, data: action.payload}
        case BID_DATA_RESET:
            return {...state, ...INITAL_STATE}
        default:
            return state
    }
}
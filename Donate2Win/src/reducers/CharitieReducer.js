import {
    CHARITIE_DATA,
    CREATE_CHARITIE,
    CREATE_CHARITIE_SUCCESS,
    CREATE_CHARITIE_FAIL,
    EDIT_CHARITIE_FAIL,
    EDIT_CHARITIE_SUCCESS,
    DATA_CHARITIE_SEARCHED
} from '../actions/types'
const INITAL_STATE={
    loading: false,
    error: '',
    data:'',
    dataCopy:''
}

export default (state = INITAL_STATE, action)=>{
    switch(action.type){
        case CHARITIE_DATA:
            return  {data:action.payload , dataCopy:action.payload}
        case CREATE_CHARITIE:
            return {...state, loading: true, error: ''}
        case CREATE_CHARITIE_SUCCESS:
            return  {...state, ...INITAL_STATE}
        case CREATE_CHARITIE_FAIL:
            return  {...state, loading: false,error: 'Nu merge'}
        case EDIT_CHARITIE_FAIL:
            return  {...state, loading: false,error: 'Nu merge'}
        case EDIT_CHARITIE_SUCCESS:
            return  {...state, ...INITAL_STATE}
        case DATA_CHARITIE_SEARCHED:
            return {...state, dataCopy: action.payload}
        default:
            return state
    }
}
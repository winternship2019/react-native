import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer'
import UserReducer from './UserReducer'
import CharitieReducer from './CharitieReducer'
import AccountReducer from './AccountReducer'
import ProductReducer from './ProductReducer'
import BidReducer from './BidReducer'

export default combineReducers({
    auth: AuthReducer,
    account: UserReducer,
    charities: CharitieReducer,
    admin: AccountReducer,
    product: ProductReducer,
    bid: BidReducer
})
import React from 'react';
import { Scene, Router, Actions} from 'react-native-router-flux';
import WaitStart from './component/SplashScreen/WaitStart';

import PostsForm from './component/PostsPage/PostsForm';
import CreateProductForm from './component/PostsPage/CreateProductForm';
import ProductDetail from './component/PostsPage/ProductDetail';
import EditProductForm from './component/PostsPage/EditProductForm';

import CreateCharitieForm from './component/CharitiesPage/CreateCharitieForm';
import CharitieDetail from './component/CharitiesPage/CharitieDetail';
import EditCharitieForm from './component/CharitiesPage/EditCharitieForm';
import CharitiesForm from './component/CharitiesPage/CharitiesForm';

import UsersForm from './component/UsersPage/UsersForm';
import CreateAccountForm from './component/UsersPage/CreateAccountForm';
import UserDetail from './component/UsersPage/UserDetail';
import EditUserForm from './component/UsersPage/EditUserForm';

import AccountPage from './component/AccountPage/AccountForm';
import EditAccountForm from './component/AccountPage/EditAccountForm';

import LogInForm from './component/LogInPage/LogInForm';
import ResetPassword from './component/LogInPage/ResetPassword';

const RouterComponent =() =>{
    return(
        <Router>
            <Scene key="root" hideNavBar>
                <Scene key="load" hideNavBar>
                    <Scene
                        key="splashScreen" 
                        component={WaitStart} 
                        title="Donate2Win"
                    />
                </Scene>
                <Scene key="auth">
                    <Scene
                        key="login" 
                        component={LogInForm} 
                        title="LogIn"
                        hideNavBar
                        initial
                    />
                    <Scene
                        key="resetPassword" 
                        component={ResetPassword} 
                        title="ResetPassword"
                        hideNavBar
                    />
                </Scene>
                <Scene key='postMain' hideNavBar>
                    <Scene
                        hideNavBar
                        key="posts"
                        component={PostsForm}
                        initial
                    />
                    <Scene
                        key="createProduct"
                        component={CreateProductForm}
                        title="Create Product"
                    />
                    <Scene
                        title="Edit Product"
                        key="editProduct"
                        component={EditProductForm}
                    />
                    <Scene
                        title="Product Page"
                        key="productDetail"
                        component={ProductDetail}
                    />
                </Scene>
                <Scene key='userMain' hideNavBar>
                    <Scene
                        key="users"
                        component={UsersForm}
                        title="Users"
                        initial
                    />
                    <Scene
                        key="createAccount"
                        component={CreateAccountForm}
                        title="Create Account"
                    />
                    <Scene
                        title="Edit User"
                        key="editUser"
                        component={EditUserForm}
                    />
                    <Scene
                        title="User Page"
                        key="userDetail"
                        component={UserDetail}
                    />
                </Scene>
                <Scene key="charitieMain" hideNavBar>
                        <Scene
                            title="Charities"
                            key="charitie"
                            component={CharitiesForm}
                            initial
                            hideNavBar
                        />
                        <Scene
                            key="createCharitie"
                            component={CreateCharitieForm}
                            title="Create Charities"
                            
                        />
                        <Scene
                            title="Edit Charities"
                            key="editCharitie"
                            component={EditCharitieForm}
                        />
                        <Scene
                            title="Charities Page"
                            key="charitieDetail"
                            component={CharitieDetail}
                        />
                    </Scene>
                <Scene key='accountMain'>
                    <Scene
                            key="account"
                            component={AccountPage}
                            title="Account"
                            initial
                            hideNavBar
                    />
                    <Scene
                            key="editAccount"
                            component={EditAccountForm}
                            title="Account"
                            hideNavBar
                    />
                </Scene>
            </Scene>
        </Router>
    )
}
export default RouterComponent
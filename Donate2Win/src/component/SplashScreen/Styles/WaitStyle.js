import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    backgroundImageSize:{
        width: width, 
        height: height,
        backgroundColor: '#FAFAFA'
    },
    imageBackgroundImage:{
        resizeMode:'stretch',
    },
    mainScreen:{
        flex: 1,
    },
    titleStyle:{
        height: moderateScale(127,0.1),
        width: moderateScale(200,0.1),
        marginTop:moderateScale(270,0.1),
        alignSelf:'center'
    }
})
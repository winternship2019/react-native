import React, {Component} from 'react';
import {Spinner} from '../common/index'
import {View, ImageBackground,Image} from 'react-native';
import styles from './Styles/WaitStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
import TimerMixin from 'react-timer-mixin';
import {productsData,charitiesData} from '../../actions';
import {connect} from 'react-redux';
class WaitStart extends Component{
    componentDidMount(){
        this.props.productsData()
        this.props.charitiesData()
        TimerMixin.setTimeout(
            () => { Actions.auth() },
            3000
          );
    }
    render(){
        return(
            <View style={styles.mainScreen}>
                <ImageBackground  
                    source={images.background} 
                    imageStyle={styles.imageBackgroundImage}
                    style={styles.backgroundImageSize}
                >
                <Image source={images.splash} style={styles.titleStyle}/>
                <Spinner size="large"/>
                </ImageBackground>
            </View>
            
        )
    }
}
export default connect(null,{productsData,charitiesData})(WaitStart)

import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    title1Style:{
        color: 'black',
        fontSize: moderateScale(34),
        textAlign: 'left',
        fontFamily: 'SF Pro Display',
        fontWeight: 'bold',
        letterSpacing: 0.41,
        lineHeight: 41,
        opacity: 100,
        fontStyle: 'normal',
        marginTop: moderateScale(173),
        marginLeft: moderateScale(16),
    },
    title2Style:{
        color: '#E34256',
        fontSize: moderateScale(34),
        textAlign: 'left',
        fontFamily: 'SF Pro Display',
        fontWeight: 'bold',
        letterSpacing: 0.41,
        lineHeight: 41,
        opacity: 100,
        fontStyle: 'normal',
    },
    backgroundImageSize:{
        width: width, 
        height: height,
        backgroundColor: '#FAFAFA'
    },
    imageBackgroundImage:{
        resizeMode:'stretch',
    },
    mainScreen:{
        flex: 1,
    },
    inputPasswordStyle:{
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        marginLeft: moderateScale(16),
        opacity: 100, 
        flexDirection: 'column',       
    },
    inputStyle:{
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(30),
        flexDirection: 'column',
        opacity: 100,        
    },
    textInputStyle:{
        color: '#8A8A8F',
        fontSize: moderateScale(15),
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
    },
    buttonStyle:{
        marginTop: moderateScale(23),
        marginLeft: moderateScale(16),
        marginRight: moderateScale(16),
        borderColor: '#3F4653',
        borderRadius: moderateScale(8),
        height: moderateScale(50),
        backgroundColor: '#3F4553',
        opacity: 100,     
    },
    buttonTextStyle:{
        fontSize: moderateScale(17),
        color: '#FFFFFF',
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight:'bold',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        fontFamily: 'SF Pro Text',
        fontStyle: 'normal',
        paddingTop: moderateScale(15)
    },
    explainText:{
        width: width,
        fontSize: moderateScale(15),
        color: '#8C8C8C',
        fontWeight:'normal',
        letterSpacing: -0.24,
        lineHeight: 18,
        opacity: 100,
        fontFamily: 'SF Pro Text',
        fontStyle: 'normal',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(7)
    },
    signInView:{
        position: 'absolute',
        bottom: 0,
        marginBottom: moderateScale(40),
        marginLeft: moderateScale(16),
        flexDirection: 'row',       
    },
    signInText1:{
        fontSize: moderateScale(15),
        color: '#8C8C8C',
        fontWeight:'normal',
        letterSpacing: -0.24,
        lineHeight: 18,
        opacity: 100,
        fontFamily: 'SF Pro Text',
        fontStyle: 'normal',
    },
    signInText2:{
        fontSize: moderateScale(15),
        color: '#E34256',
        fontWeight:'normal',
        letterSpacing: -0.24,
        lineHeight: 18,
        opacity: 100,
        fontFamily: 'SF Pro Text',
        fontStyle: 'normal',
    },
})
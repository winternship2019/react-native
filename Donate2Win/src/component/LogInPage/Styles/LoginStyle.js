import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    title1Style:{
        color: 'black',
        fontSize: moderateScale(34),
        textAlign: 'left',
        fontFamily: 'SF Pro Display',
        fontWeight: 'bold',
        letterSpacing: 0.41,
        lineHeight: 41,
        opacity: 100,
        fontStyle: 'normal',
        marginTop: moderateScale(183),
        marginLeft: moderateScale(16),
    },
    title2Style:{
        color: '#E34256',
        fontSize: moderateScale(34),
        textAlign: 'left',
        fontFamily: 'SF Pro Display',
        fontWeight: 'bold',
        letterSpacing: 0.41,
        lineHeight: 41,
        opacity: 100,
        fontStyle: 'normal',
    },
    backgroundImageSize:{
        width: width, 
        height: height,
        backgroundColor: '#FAFAFA'
    },
    imageBackgroundImage:{
        resizeMode:'stretch',
    },
    mainScreen:{
        flex: 1,
    },
    inputPasswordStyle:{
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        marginLeft: moderateScale(16),
        opacity: 100, 
        flexDirection: 'column',       
    },
    inputStyle:{
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(30),
        flexDirection: 'column',
        opacity: 100,        
    },
    textInputStyle:{
        color: '#8A8A8F',
        fontSize: moderateScale(15),
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
    },
    buttonStyle:{
        marginTop: moderateScale(23),
        marginLeft: moderateScale(16),
        marginRight: moderateScale(16),
        borderColor: '#3F4653',
        borderRadius: moderateScale(8),
        height: moderateScale(50),
        backgroundColor: '#3F4553',
        opacity: 100,     
    },
    buttonTextStyle:{
        fontSize: moderateScale(17),
        color: '#FFFFFF',
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight:'bold',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        fontFamily: 'SF Pro Text',
        fontStyle: 'normal',
        paddingTop: moderateScale(15)
    },
    forgotStyle:{
        marginLeft: moderateScale(16),
        width:moderateScale(110),
        marginTop: moderateScale(5)
    },
    forgotTextStyle:{
        fontSize: moderateScale(15),
        color: '#E34256',
        textAlign: 'left',
        letterSpacing: -0.36,
        lineHeight: 22,
        opacity: 100,
        fontFamily: 'SF Pro Text',
        fontStyle: 'normal',
        width:moderateScale(200)
    },
})
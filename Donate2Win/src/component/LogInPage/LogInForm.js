import React, {Component} from 'react';
import {ScrollView,Alert,BackHandler,View, Text, ImageBackground} from 'react-native';
import {Input,Button,Spinner} from '../common/index'
import {connect} from 'react-redux';
import {loginAdmin,isError} from '../../actions/index';
import styles from './Styles/LoginStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
class LogInForm extends Component{
    constructor(){
        super()
        this.state={
            email:'',
            password:'',
        }
    }
    onLoginFail=()=>{
        if(this.props.error)
            return(
                Alert.alert(
                    "Log in",
                    'Incorrect username or password.',
                    [
                        {text:'Ok', onPress:()=>{this.props.isError(false)}},
                    ],
                    {cancelable: false},
                )
            )
    }
    componentWillMount(){
        BackHandler.addEventListener('backLF',()=>{
            return true;
        })
    }
    onLoginPress=()=>{
        const{email,password}= this.state
        this.props.loginAdmin({email,password})
    }
    onTryLogin=()=>{
        if(this.props.loading){
            return <Spinner size="large"/>
        }
        return(
            <Button 
                onPress={this.onLoginPress} 
                style={[styles.buttonStyle,styles.buttonTextStyle]}
            >
                Log in 
            </Button>
        )
    }
    render(){
        
        return(
            <View style={styles.mainScreen}>
            <ScrollView>
                <ImageBackground  
                    source={images.background} 
                    imageStyle={styles.imageBackgroundImage}
                    style={styles.backgroundImageSize}
                >
                    <Text style={styles.title1Style}>
                        {'give a win \n'}
                        <Text style={styles.title2Style}>
                            {'sign in.'}
                        </Text>
                    </Text>
                    <Input 
                        label="Email adress"
                        placeholder=""
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                        style={[styles.inputStyle,styles.textInputStyle]}
                    />
                    <Input 
                        secureTextEntry
                        label="Password"
                        placeholder=""
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}
                        style={[styles.inputPasswordStyle,styles.textInputStyle]}
                    />              
                    <Button 
                        onPress={()=>{Actions.resetPassword()}} 
                        style={[styles.forgotStyle,styles.forgotTextStyle]}
                    >
                        Forgot password?
                    </Button>
                    {this.onLoginFail()}
                    {this.onTryLogin()}
                    
                </ImageBackground>
                </ScrollView>
            </View>
        )
    }
}
const mapStateToProps=({auth}) =>{
    const {error,loading}=auth
    return {error,loading}
}
export default connect(mapStateToProps,{isError,loginAdmin})(LogInForm)

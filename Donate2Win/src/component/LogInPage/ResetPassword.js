import React, {Component} from 'react';
import {ScrollView,Alert,BackHandler,View, Text, ImageBackground, TouchableOpacity} from 'react-native';
import {Input,Button,Spinner} from '../common/index'
import {connect} from 'react-redux';
import {resetPassword,resetError} from '../../actions/index';
import styles from './Styles/ResetStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
class ResetPassword extends Component{
    constructor(){
        super()
        this.state={
            email:'',
        }
    }
    componentWillMount(){
        BackHandler.addEventListener('backPD',()=>{
            Actions.auth()
            return true;
        })
    }
    onResetFail=()=>{
        if(this.props.error)
            return(
                Alert.alert(
                    'Reset password',
                    'Incorrect email',
                    [
                        {text:'ok', onPress:()=>{this.props.resetError(false)}}
                    ],
                    {cancelable: false},
                )
            )
    }
    onResetPress=()=>{
        const{email}= this.state
        this.props.resetPassword(email)
    }
    onTryReset=()=>{
        if(this.props.loading){
            return <Spinner size="large"/>
        }
        return(
            <Button 
                onPress={this.onResetPress} 
                style={[styles.buttonStyle,styles.buttonTextStyle]}
            >
                Reset password
            </Button>
        )
    }
    render(){
        
        return(
            <View style={styles.mainScreen}>
            <ScrollView>
                <ImageBackground  
                    source={images.background} 
                    imageStyle={styles.imageBackgroundImage}
                    style={styles.backgroundImageSize}
                >
                    <Text style={styles.title1Style}>
                        {'I keep \n'}
                        <Text style={styles.title2Style}>
                            {'forgeting.'}
                        </Text>
                    </Text>
                    <Text style={styles.explainText}>
                        {'So it’s easy peasy, just type your email adress \n'}
                        {'and we’ll reset it for you'}
                    </Text>
                    <Input 
                        label="Email adress"
                        placeholder=""
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                        style={[styles.inputStyle,styles.textInputStyle]}
                    />            
                    {this.onResetFail()}
                    {this.onTryReset()}
                    <View style={styles.signInView}>
                        <Text style={styles.signInText1}>
                            {'You remember quick! '}      
                        </Text>
                        <TouchableOpacity onPress={()=>{Actions.login()}}>
                            <Text style={styles.signInText2}>
                                {'Sign in!'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
                </ScrollView>
            </View>
        )
    }
}
const mapStateToProps=({admin}) =>{
    const {error,loading}=admin
    return {error,loading}
}
export default connect(mapStateToProps,{resetError,resetPassword})(ResetPassword)

import React, {Component} from 'react';
import {View, Image, Text,TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {deleteUser,bidData} from '../../actions';
import { Actions } from 'react-native-router-flux';
import styles from './Styles/UserItemStyle'
class UserItem extends Component {
    onRowPress(){
        this.props.bidData(this.props.token,this.props.data._id)
        Actions.userDetail({user: this.props.data})
    }
    render(){
        const{name, wallet,img_url} = this.props.data
        return (
            <TouchableOpacity onPress={()=>{this.onRowPress()}}>
            <View style={styles.mainView}>
                <Image source={{uri: img_url}} style={styles.imageStyle} />
                <View style={styles.textView}>
                    <Text style={styles.titleStyle}>
                        {name}
                    </Text>
                    <Text style={styles.descriptionStyle}>
                        {wallet + ' lei'}
                    </Text>
                </View>
            </View>
             </TouchableOpacity>
        )
    }
}
const mapStateToProps = ({auth}) =>{
    const token=auth
    return {token}
}
export default connect(mapStateToProps,{bidData,deleteUser})(UserItem)
import React, {Component} from 'react';
import {BackHandler,ScrollView,View,Text,Image,FlatList,TouchableOpacity,ImageBackground} from 'react-native'
import styles from './Styles/UserDetailStyle'
import images from '../../images'
import {deleteUser,bidData} from '../../actions';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux'
import BidItem from './BidItem'
class UserDetail extends Component{
    renderRow(data){
        return <BidItem data={data.item} />
    }
    componentWillMount(){
        BackHandler.addEventListener('backUD',()=>{
            Actions.userMain()
            return true;
        })
    }
    moneySpent(){
        suma = 0
        for(i=0;i<this.props.data.length;i++){
            suma+=this.props.data[i].product.bid_price
        }
        return suma
    }
    existBids(){
        if(this.props.data.length==0)
            return "Don't have bids"
        else
            return "User bids"
    }
    render(){
        const{name, email,role,wallet,img_url,_id} = this.props.user
        return(
            <View style={styles.mainView}>
            <ScrollView>
                <ImageBackground source={images.backgroundProducts} style={styles.backgroundImageSize}>
                    <View style={styles.bigHeader}>
                        <View style={styles.headerView}>
                            <TouchableOpacity onPress={()=>{Actions.userMain()}} style={styles.headerPosition}>
                                <Image source={images.backPinkArrow} style={styles.imageBack}/>
                                <Text style={styles.cancelStyle}>
                                    Users
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>{Actions.editUser({user: this.props.user})}} style={styles.headerPosition}>
                                <Text style={styles.editStyle}>
                                    Edit
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.imageViewStyle}>
                            <Image source={{uri: img_url}} style={styles.imageStyle}/>
                        </View>
                        <Text style={styles.textInfoEdit}>
                            {name}
                        </Text>
                        <Text style={styles.textAboutEdit}>
                            General info
                        </Text>
                    </View>
                </ImageBackground>    
                <View style={styles.generalDetailsView}>
                    <View style={styles.infoView}>
                        <Text style={styles.textDetail}>
                            Bids spent
                        </Text>
                        <Text style={styles.textInfoStyle}>
                            {this.props.data.length}
                        </Text>
                    </View>
                    <View style={styles.infoView}>
                        <Text style={styles.textDetail}>
                            Money spent
                        </Text>
                        <Text style={styles.textInfoStyle}>
                            {this.moneySpent()}
                        </Text>
                    </View>
                    <View style={styles.infoView}>
                        <Text style={styles.textDetail}>
                            Wallet
                        </Text>
                        <Text style={styles.textInfoStyle}>
                            {wallet}
                        </Text>
                    </View>
                </View>               
                <View>
                    <Text style={styles.textAboutEdit}>
                        Account info
                    </Text>
                    <View style={styles.generalDetailsView}>
                        <View style={styles.infoView}>
                            <Text style={styles.textDetail}>
                                Display name
                            </Text>
                            <Text style={styles.textInfoStyle}>
                                {name}
                            </Text>
                        </View>
                        <View style={styles.infoView}>
                            <Text style={styles.textDetail}>
                                Email
                            </Text>
                            <Text style={styles.textInfoStyle}>
                                {email}
                            </Text>
                        </View>
                        <View style={styles.infoView}>
                            <Text style={styles.textDetail}>
                                Role
                            </Text>
                            <Text style={styles.textInfoStyle}>
                                {role}
                            </Text>
                        </View>
                    </View>  
                </View>
                <View>
                    <Text style={styles.textBids}>
                        {this.existBids()}
                    </Text>
                    <View style={styles.listView}>
                        <FlatList
                            data={this.props.data}
                            renderItem={this.renderRow}
                            keyExtractor={data => data._id}
                        />
                    </View>
                </View>
                <View style={styles.comandsStyle}>
                    <TouchableOpacity onPress={()=>Actions.editUser({user: this.props.user})}>
                        <Text style={styles.modifyText}>
                            Edit user
                        </Text>
                    </TouchableOpacity>
                    <View style={styles.barStyle}/>
                    <TouchableOpacity onPress={()=>{this.props.deleteUser(this.props.token,_id)}}>
                        <Text style={styles.modifyText}>
                            Delete user
                        </Text>
                    </TouchableOpacity>                               
                </View>     
            </ScrollView>   
        </View>
        )
    }
}
const mapStateToProps = ({auth,bid}) =>{
    const token=auth
    const {data}=bid
    return {token,data}
}
export default connect(mapStateToProps,{deleteUser,bidData})(UserDetail)
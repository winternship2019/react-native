import React, {Component} from 'react';
import {Card, Button, CardSection, Input, Bool, Spinner} from '../common/index';
import {Alert,BackHandler,TextInput,ScrollView,View,Text,Image,TouchableOpacity,ImageBackground} from 'react-native'
import {connect} from 'react-redux';
import {createAccount} from '../../actions/index'
import ImagePicker from 'react-native-image-picker'
import {Actions} from 'react-native-router-flux'
import styles from './Styles/CreateUserStyle'
import images from '../../images'
class CreateAccountForm extends Component{
    constructor(){
        super()
        this.state={
            name: '', 
            email: '',
            isAdmin: false,
            role: 'user',
            wallet: 0,
            image: 'https://i.imgur.com/lsG4cTC.png',
            password:'',
            passwordC:'',
            userImage:{fileName:'',type:'',path: ''}
        }
    }
    componentWillMount(){
        BackHandler.addEventListener('backUC',()=>{
            Actions.userMain()
            return true;
        })
    }
    onSelectImage=()=>{
        const options = {
            noData:true
        }
        ImagePicker.launchImageLibrary(options, response => {
            if(response){
                this.setState({image: response.uri})
                this.setState({userImage:response})
            }
        });
    }
    adminAccount(){
        if(this.state.isAdmin)
            this.state.role='admin'
        else
            this.state.role='user'
    }
    onBoolPress(){
        if(this.state.isAdmin){
            return(
                <View style={styles.isAdminStyle}>
                    <View style={styles.boolStyleTrue}/>
                </View>
            )
        }
        else{
            return (
                <View style={styles.notAdminStyle}>
                    <View style={styles.boolStyleFalse}/>
                </View>
            )
        }
    }
    onCreatePress=()=>{ 
        if(this.state.password==this.state.passwordC)
        {
            const{name,email,password,role,wallet,userImage}=this.state
            this.props.createAccount({name,email,password,role,wallet,userImage},this.props.token)
        }
        else
            return(
                Alert.alert(
                    "Try again",
                    'Passwords are not matching.',
                    [
                        {text:'Ok'},
                    ],
                    {cancelable: false},
                )
            )  
    }
    render(){
        return(
            <View style={styles.mainView}>
            <ScrollView>
                <ImageBackground source={images.backgroundProducts} style={styles.backgroundImageSize}>
                    <View style={styles.bigHeader}>
                        <View style={styles.headerView}>
                            <TouchableOpacity onPress={()=>{Actions.userMain()}} style={styles.headerPosition}>
                                <Image source={images.backPinkArrow} style={styles.imageBack}/>
                                <Text style={styles.cancelStyle}>
                                    Cancel
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.onCreatePress} style={styles.headerPosition}>
                                <Text style={styles.addStyle}>
                                    Save changes
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.imageViewStyle}>
                            <TouchableOpacity onPress={this.onSelectImage}>
                                <Image source={{uri: this.state.image}} style={styles.imageStyle}/>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.imageDescription}>
                            Be a human, have a face
                        </Text>
                        <Text style={styles.textInfoEdit}>
                            Edit account info
                        </Text>
                    </View>
                </ImageBackground>
                <View style={styles.editView}>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Display name
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder='My name'
                            autoCorrect={false}
                            value={this.state.name}
                            onChangeText={(name) => this.setState({name})}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Email
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder='exemple@gmail.com'
                            autoCorrect={false}
                            onChangeText={(email) => this.setState({email})}
                            value={this.state.email}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Wallet  
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder='0'
                            autoCorrect={false}
                            onChangeText={(wallet) => this.setState({wallet})}
                            value={`${this.state.wallet}`}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            New password
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={true}
                            placeholder='password'
                            autoCorrect={false}
                            value={this.state.password}
                            onChangeText={(password) => this.setState({password})}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Confirm password
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={true}
                            placeholder='password'
                            autoCorrect={false}
                            onChangeText={(passwordC) => this.setState({passwordC})}
                            value={this.state.passwordC}
                        />
                    </View>
                </View>    
                <View style={styles.adminButton}>
                    <TouchableOpacity style={styles.buttonBackground} onPress={()=> this.setState({ isAdmin: !this.state.isAdmin })}>
                        <Text style={styles.modifyText}>
                            Admin
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={1} style={styles.buttonBackground} onPress={()=> this.setState({ isAdmin: !this.state.isAdmin })}>
                        {this.onBoolPress()}
                    </TouchableOpacity>
                    {this.adminAccount()}
                </View>
            </ScrollView>
        </View>
        )
    }
}
const mapStateToProps=({account,auth})=>{
    const {name,email,password,passwordConfirmed,error,loading,role}=account
    const{token}=auth
    return {name,email,password,passwordConfirmed,error,loading,role,token}
}
export default connect(mapStateToProps,{createAccount})(CreateAccountForm)
import React, {Component} from 'react';
import {Alert,BackHandler,TextInput,ScrollView,View,Text,Image,TouchableOpacity,ImageBackground} from 'react-native'
import {Spinner, Button} from '../common/index';
import {editUser,deleteUser} from '../../actions';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker'
import {Actions} from 'react-native-router-flux'
import styles from './Styles/EditUserStyle'
import images from '../../images'
class EditUserForm extends Component{
    constructor(){
        super()
        this.state={
            name: '', 
            email: '',
            isAdmin: false,
            role: '',
            wallet: 0,
            image: '',
            passwordO:'',
            password:'',
            passwordC:'',
            userImage:{fileName:'',type:'',path: ''}
        }
    }
    firstValue=()=>{
        const{name,email,role,img_url,wallet}=this.props.user
        this.state.name=name
        this.state.email=email
        this.state.image=img_url
        this.state.role=role
        this.state.wallet=wallet
        if(role=="admin")
            this.state.isAdmin=true
    }
    onSelectImage=()=>{
        const options = {
            noData:true
        }
        ImagePicker.launchImageLibrary(options, response => {
            if(response){
                this.setState({image: response.uri})
                this.setState({userImage:response})
            }
        });
    }
    onTryCreateAccount(){
        if(this.props.loading){
            return <Spinner size="large"/>
        }
        return(
            <Button onPress={this.onEditDone}>
                Done
            </Button>
        )
    }
    onCreateFail(){
        if(this.props.error)
            return(
                <View>
                    <Text style={{color:'red'}}>
                        {this.props.error}
                    </Text>
                </View>
            )
    }
    onEditDone=()=>{
        if(this.state.password==this.state.passwordC && this.state.wallet>=0)
        {
            name=this.state.name
            email=this.state.email
            img_url=this.state.userImage
            role=this.state.role
            wallet=this.state.wallet
            password=this.state.password
            this.props.editUser(this.props.token,this.props.user._id,{name,email,img_url,role,wallet,password})
        }
        else
            if(this.state.wallet<0)
                return(    
                    Alert.alert(
                        "Try again",
                        'Incorrect wallet.',
                        [
                            {text:'Ok'},
                        ],
                        {cancelable: false},
                    )
                )
            else
                return(
                    Alert.alert(
                        "Try again",
                        'Passwords are not matching.',
                        [
                            {text:'Ok'},
                        ],
                        {cancelable: false},
                    )
                )  
    }
    componentWillMount(){
        this.firstValue()
        BackHandler.addEventListener('backUDetail',()=>{
            Actions.userDetail({user: this.props.user})
            return true;
        })
    }
    onBoolPress(){
        if(this.state.isAdmin){
            return(
                <View style={styles.isAdminStyle}>
                    <View style={styles.boolStyleTrue}/>
                </View>
            )
        }
        else{
            return (
                <View style={styles.notAdminStyle}>
                    <View style={styles.boolStyleFalse}/>
                </View>
            )
        }
    }
    adminAccount(){
        if(this.state.isAdmin)
            this.state.role='admin'
        else
            this.state.role='user'
    }
    render(){
        return(
            <View style={styles.mainView}>
            <ScrollView>
                <ImageBackground source={images.backgroundProducts} style={styles.backgroundImageSize}>
                    <View style={styles.bigHeader}>
                        <View style={styles.headerView}>
                            <TouchableOpacity onPress={()=>{Actions.userDetail({user: this.props.user})}} style={styles.headerPosition}>
                                <Image source={images.backPinkArrow} style={styles.imageBack}/>
                                <Text style={styles.cancelStyle}>
                                    Cancel
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.onEditDone} style={styles.headerPosition}>
                                <Text style={styles.addStyle}>
                                    Save changes
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.imageViewStyle}>
                            <TouchableOpacity onPress={this.onSelectImage}>
                                <Image source={{uri: this.state.image}} style={styles.imageStyle}/>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.imageDescription}>
                            Be a human, have a face
                        </Text>
                        <Text style={styles.textInfoEdit}>
                            Edit account info
                        </Text>
                    </View>
                </ImageBackground>
                <View style={styles.editView}>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Display name
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            value={this.state.name}
                            onChangeText={(name) => this.setState({name})}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Email
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(email) => this.setState({email})}
                            value={this.state.email}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Wallet  
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(wallet) => this.setState({wallet})}
                            value={`${this.state.wallet}`}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Old password
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={true}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(passwordO) => this.setState({passwordO})}
                            value={this.state.passwordO}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            New password
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={true}
                            placeholder=''
                            autoCorrect={false}
                            value={this.state.password}
                            onChangeText={(password) => this.setState({password})}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Confirm password
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={true}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(passwordC) => this.setState({passwordC})}
                            value={this.state.passwordC}
                        />
                    </View>
                </View>    
                <View style={styles.adminButton}>
                    <TouchableOpacity style={styles.buttonBackground} onPress={()=> this.setState({ isAdmin: !this.state.isAdmin })}>
                        <Text style={styles.modifyText}>
                            Admin
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={1} style={styles.buttonBackground} onPress={()=> this.setState({ isAdmin: !this.state.isAdmin })}>
                        {this.onBoolPress()}
                    </TouchableOpacity>
                    {this.adminAccount()}
                </View>
                <View style={styles.buttonsView}>
                    <TouchableOpacity style={styles.buttonBackground} onPress={this.onEditDone}>
                        <Text style={styles.modifyText}>
                            Save changes
                        </Text>
                    </TouchableOpacity>
                    <View style={styles.barStyle}/>
                </View> 
            </ScrollView>
        </View>
        )
    }
}
const mapStateToProps = ({auth,account}) =>{
    const token=auth
    const{error,loading}=account
    return {token,error,loading}
}
export default connect(mapStateToProps,{editUser,deleteUser})(EditUserForm)
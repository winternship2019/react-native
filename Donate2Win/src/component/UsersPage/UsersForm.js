import React, {Component} from 'react';
import {BackHandler,Image,Text,TouchableOpacity,ImageBackground,View,TextInput, FlatList} from 'react-native';
import styles from './Styles/UserFormStyle'
import images from '../../images'
import UserItem from './UserItem'
import {usersData,userSearched,resetData} from '../../actions';
import {connect} from 'react-redux';
import { BottomBar } from '../common';
import { Actions } from 'react-native-router-flux';

class UsersForm extends Component{
    constructor(){
        super()
        this.state={
            search:'',
        }
    }
    componentWillMount(){
        BackHandler.addEventListener('backUF',()=>{
            Actions.userMain()
            return true;
        })
    }
    componentDidMount(){
        this.props.usersData(this.props.token)
        this.props.resetData()
    }

    searchUser(search){
        const searched=[]
        this.setState({search: search})

        for(i=0;i<this.props.data.length;i++){
            if(this.props.data[i].name.includes(search) || this.props.data[i].email.includes(search)){
                searched.push(this.props.data[i])
            }
        }
        this.props.userSearched(searched)
        
    }
    renderRow(data){
        return <UserItem data={data.item} />
    }
    render(){
        return(
            <View style={styles.mainView}>
                <View style={styles.headerPart}>
                    <View style={styles.headerView}>
                        <TouchableOpacity>
                            <Text style={styles.editText}>
                                Edit
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Actions.createAccount()}}>
                            <Image source={images.addProductIcon} style={styles.addText}/>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.allProdText}>
                        All users
                    </Text>
                    <View style={styles.searchBarView}>
                        <Image source={images.searchIcon} style={styles.imageSearch}/>
                        <TextInput
                            style={styles.textSearch}
                            secureTextEntry={false}
                            placeholder='Search'
                            autoCorrect={false}
                            value={this.state.search}
                            onChangeText={(search) => {
                                this.searchUser(search)
                            }}
                            maxLength={30}
                        />
                    </View>
                </View>
                <View style={styles.productsView}>
                    <ImageBackground  
                        source={images.backgroundProducts} 
                        style={styles.backgroundImageSize}
                    >
                        <FlatList
                            data={this.props.dataCopy}
                            renderItem={this.renderRow}
                            keyExtractor={data => data._id}
                        />
                    </ImageBackground>
                </View>       
                
                <BottomBar user={true} account={false} charitie={false} product={false}/>
            </View>
        )
    }
}
const mapStateToProps = ({account,auth}) =>{
    const {data,dataCopy}=account
    const token=auth
    return {data,token,dataCopy}
}
export default connect(mapStateToProps,{userSearched,usersData,resetData})(UsersForm)

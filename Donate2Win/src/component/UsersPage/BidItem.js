import React, {Component} from 'react';
import {View, Image, Text,TouchableWithoutFeedback} from 'react-native';
import styles from '../PostsPage/Styles/ProductItemStyle'
class BidItem extends Component {
    getData(endDate){
        currentDay= new Date()
        endingDay= new Date(endDate)
        if(currentDay.getTime()<endingDay.getTime())
        {
            return 'Active'
        }
        else
            return 'Suspended'
    }
    onWin(won){
        const{end_date,bid_price} = this.props.data.product
        if(won)
            return('win')
        if(!won && this.getData(end_date)==='Suspended')
            return('lost')
        return bid_price + ' lei'
        
    }
    render(){
        const{
            name,
            img_url,
            bid_price,
            reserve_amount,
            bid_counter,
            end_date,
            won,
        } = this.props.data.product
        return (
                <View style={styles.viewProduct}>
                    <View style={styles.viewImage}>
                        <Image style={styles.imageStyle} source={{uri: img_url}}/>
                    </View>
                    <View style={styles.viewDetails}>
                        <Text style={styles.nameStyle}>
                            {name}
                        </Text>
                        <Text style={styles.nameStyle}>
                            {this.getData(end_date)}
                        </Text>
                        <Text style={styles.detailStyle}>
                            {bid_counter} bids
                        </Text>
                        <Text style={styles.detailStyle}>
                            {(((bid_price*bid_counter)/reserve_amount)*100).toFixed(2)+ '% out of'}  
                            <Text style={styles.littlePriceStyle}>
                                { ' '+reserve_amount} lei
                            </Text>
                        </Text>
                    </View>
                    <View style={styles.viewPrice}>
                        <Text style={styles.textPriceStyle}>
                            {this.onWin(won)}
                        </Text>
                    </View> 
                </View>
        )
    }
}
export default BidItem
import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    mainView:{
        flexDirection: 'column',
        opacity: 100,
        flex:1,
        backgroundColor: '#F7F7F7',
    },
    headerPart:{
        position: 'absolute',
        width: width,
        flexDirection: 'column',
    },
    headerView:{
        position: 'absolute',
        height: moderateScale(64),
        flexDirection: 'row',
        marginTop: moderateScale(5)
    },
    editText:{
        color:'#E34256',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        paddingTop:moderateScale(15),
        paddingLeft:moderateScale(12)
    },
    addText:{
        opacity: 100,
        marginTop: moderateScale(15),
        marginLeft: moderateScale(300)
    },
    allProdText:{
        color: 'black',
        fontSize: 34,
        textAlign: 'left',
        fontFamily: 'SF Pro Display',
        fontWeight: 'bold',
        letterSpacing: 0.41,
        lineHeight: 41,
        opacity: 100,
        fontStyle: 'normal',
        marginTop: moderateScale(67),
        marginLeft: moderateScale(19),
    },
    categoriesView:{
        marginTop: moderateScale(30),
        marginLeft: moderateScale(16),
        borderRadius: 4,
        borderWidth:1,
        height:34,
        width: width-moderateScale(32),
        borderColor: '#3F4553',
        flexDirection: 'row',
    },
    activeText:{
        alignSelf: 'center',
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        paddingTop:moderateScale(5),
        width: (width-moderateScale(38))/3
    },
    inactiveText:{
        alignSelf: 'center',
        color: '#3F4553',
        fontSize: 15,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        paddingTop:moderateScale(5),
        width: (width-moderateScale(38))/3
    },
    activeButtonCat:{
        backgroundColor:'#3F4553'
    },
    inactiveButtonCat:{
        backgroundColor:'#F7F7F7'
    },
    productsView:{
        paddingTop: moderateScale(222)
    },
    backgroundImageSize:{
        width: width, 
        height: height - moderateScale(292),
        backgroundColor: 'white'
    },
    searchBarView:{
        backgroundColor:'#F0F0F1',
        height:moderateScale(36),
        width: width-moderateScale(32),
        marginLeft: moderateScale(16),
        marginTop: moderateScale(70),
        flexDirection: 'row',
        borderRadius: moderateScale(10)
    },
    textSearch:{
        textAlign:'left',
        color: '#3F4553',
        fontSize: moderateScale(15),
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        marginLeft: moderateScale(7),
        height:moderateScale(39),
        width:moderateScale(270)
    },
    imageSearch:{
        height:moderateScale(14),
        width:moderateScale(14),
        marginLeft: moderateScale(10),
        marginTop: moderateScale(11)
    },
})
import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    mainView:{
        flexDirection: 'column',
        opacity: 100,
        flex:1,
    },
    backgroundImageSize:{
        height: moderateScale(275),
        backgroundColor: 'white'
    },
    bigHeader:{
        position: 'absolute',
        width: width,
        flexDirection: 'column',
    },
    headerView:{
        position: 'absolute',
        height: moderateScale(64),
        flexDirection: 'row',
    },
    headerPosition:{
        paddingTop:moderateScale(5),
        width: width/2,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    imageBack:{
        marginLeft:moderateScale(8.5)
    },
    cancelStyle:{
        color:'#3F4553',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        marginLeft: moderateScale(5.5)
    },
    addStyle:{
        color:'#E34256',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        marginLeft:moderateScale(75),
        width:moderateScale(200),
        height:moderateScale(21),
    },
    imageViewStyle:{
        alignSelf:'center',
        marginTop: moderateScale(84)
    },
    imageStyle:{
        height:moderateScale(86),
        width:moderateScale(86),
        borderRadius:moderateScale(43),
        backgroundColor: '#3F4553'
    },
    textInfoEdit:{
        color:'#000000',
        fontSize: 13,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        fontStyle: 'normal',
        letterSpacing: -0.08,
        lineHeight: 18,
        opacity: 100,
        marginLeft: moderateScale(17),
        marginTop: moderateScale(40)
    },
    imageDescription:{
        color: '#8A8A8F',
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
        marginLeft: moderateScale(120),
        marginTop: moderateScale(10)
    },
    editView:{
        backgroundColor: 'white',
        width: width,
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    inputStyle:{
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(10),
        flexDirection: 'row',
        opacity: 100,  
    },
    textInputStyle:{
        color: '#8A8A8F',
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
        marginTop: moderateScale(8),
        flex:1
    },
    inputTextStyle:{
        color: '#8A8A8F',
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
        flex:2,
        borderLeftWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    buttonsView:{
        width:width,
        marginBottom: moderateScale(20)
    },
    buttonBackground:{
        backgroundColor:'white',
    },
    modifyText:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.09,
        lineHeight: 26,
        opacity: 100,
        color:'#E34256',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(10),
        marginBottom: moderateScale(10),
    },
    barStyle:{
        marginLeft: moderateScale(16),
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    adminButton:{
        marginBottom: moderateScale(20),
        marginTop: moderateScale(20),
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    isAdminStyle:{
        borderRadius: moderateScale(31),
        backgroundColor: '#4CD964',
        height:moderateScale(31),
        width:moderateScale(51),
        marginTop:moderateScale(7),
        marginLeft:moderateScale(240)
    },
    notAdminStyle:{
        borderRadius: moderateScale(31),
        backgroundColor: 'red',
        height:moderateScale(31),
        width:moderateScale(51),
        marginTop:moderateScale(7),
        marginLeft:moderateScale(240)
    },
    boolStyleTrue:{
        height:moderateScale(31),
        width:moderateScale(31),
        borderRadius:moderateScale(31),
        alignSelf:'flex-end',
        backgroundColor:'white',
        shadowOpacity: moderateScale(10),
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height:2},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 2,
    },
    boolStyleFalse:{
        height:moderateScale(31),
        width:moderateScale(31),
        borderRadius:moderateScale(30),
        alignSelf:'flex-start',
        backgroundColor:'white',
        shadowOpacity: moderateScale(10),
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height:2},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 2,
    }
   
})
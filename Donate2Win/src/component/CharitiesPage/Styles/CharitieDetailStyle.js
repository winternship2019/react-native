import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    mainView:{
        flexDirection: 'column',
        opacity: 100,
        flex:1,
    },
    backgroundImageSize:{
        height: moderateScale(275),
        backgroundColor: 'white'
    },
    bigHeader:{
        position: 'absolute',
        width: width,
        flexDirection: 'column',
    },
    headerView:{
        position: 'absolute',
        height: moderateScale(64),
        flexDirection: 'row',
    },
    headerPosition:{
        paddingTop:moderateScale(5),
        width: width/2,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    imageBack:{
        marginLeft:moderateScale(8.5)
    },
    cancelStyle:{
        color:'#3F4553',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        marginLeft: moderateScale(5.5)
    },
    titleStyle:{
        color:'#000000',
        fontSize: 17,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        fontWeight: 'bold',
        fontStyle: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        
    },
    editStyle:{
        color:'#E34256',
        fontSize: 17,
        textAlign: 'right',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        marginLeft:moderateScale(150)
    },
    imageViewStyle:{
        alignSelf: 'center',
        marginTop: moderateScale(84)
    },
    imageStyle:{
        height:moderateScale(86),
        width:moderateScale(86),
        borderRadius:25,
        backgroundColor: '#FAFAFA'
    },
    textInfoEdit:{
        color:'#000000',
        fontSize: 30,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        fontStyle: 'normal',
        letterSpacing: 0.39,
        lineHeight: 36,
        opacity: 100,
        marginTop: moderateScale(7)
    },
    comandsStyle:{
        marginTop:moderateScale(20),
        backgroundColor: 'white',
        width: width,
        borderBottomWidth:moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    modifyText:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.09,
        lineHeight: 26,
        opacity: 100,
        color:'#E34256',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(10),
        marginBottom: moderateScale(10)
    },
    barStyle:{
        marginLeft: moderateScale(16),
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    textDescription:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.24,
        lineHeight: 18,
        opacity: 100,
        color:'#8C8C8C',
        marginTop: moderateScale(12),
        marginLeft: moderateScale(16),
        marginBottom: moderateScale(12),
    },
    descriptionView:{
        borderBottomWidth:moderateScale(0.5),
        borderColor: '#BCBBC1',
        backgroundColor: 'white'
    }
    
})
import React, {Component} from 'react';
import {BackHandler,Image,Text,TouchableOpacity,ImageBackground,View, FlatList,TextInput} from 'react-native';
import CharitieItem from './CharitieItem'
import {charitiesData,charitieSearched} from '../../actions';
import {connect} from 'react-redux';
import { BottomBar } from '../common';
import styles from './Styles/CharitieFormStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
class CharitiesForm extends Component{
    constructor(){
        super()
        this.state={
            search:'',
        }
    }
    componentWillMount(){
        BackHandler.addEventListener('backUF',()=>{
            Actions.charitieMain()
            return true;
        })
    }
    componentDidMount(){
        this.props.charitiesData()
    }
    searchProduct(search){
        const searched=[]
        this.setState({search: search})

        for(i=0;i<this.props.data.length;i++){
            if(this.props.data[i].name.includes(search)){
                searched.push(this.props.data[i])
            }
        }
        this.props.charitieSearched(searched)
        
    }
    renderRow(data){
        return <CharitieItem data={data.item} />
    }
    render(){
        return(
            <View style={styles.mainView}>
                <View style={styles.headerPart}>
                    <View style={styles.headerView}>
                        <TouchableOpacity>
                            <Text style={styles.editText}>
                                Edit
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Actions.createCharitie()}}>
                            <Image source={images.addProductIcon} style={styles.addText}/>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.allProdText}>
                        All causes
                    </Text>
                    <View style={styles.searchBarView}>
                        <Image source={images.searchIcon} style={styles.imageSearch}/>
                        <TextInput
                            style={styles.textSearch}
                            secureTextEntry={false}
                            placeholder='Search'
                            autoCorrect={false}
                            value={this.state.search}
                            onChangeText={(search) => {
                                this.searchProduct(search)
                            }}
                            maxLength={30}
                        />
                    </View>
                </View>
                <View style={styles.productsView}>
                    <ImageBackground  
                        source={images.backgroundProducts} 
                        style={styles.backgroundImageSize}
                    >
                        <FlatList
                            data={this.props.dataCopy}
                            renderItem={this.renderRow}
                            keyExtractor={data => data.name}
                        />
                    </ImageBackground>
                </View>       
                <BottomBar user={false} account={false} charitie={true} product={false}/>
            </View>
        )
    }
}
const mapStateToProps = ({charities,auth}) =>{
    const {data,dataCopy}=charities
    const token=auth
    return {data,dataCopy,token}
}
export default connect(mapStateToProps,{charitiesData,charitieSearched})(CharitiesForm)
import React, {Component} from 'react';
import {BackHandler,ScrollView,View,Text,Image,TextInput,FlatList,TouchableOpacity,ImageBackground} from 'react-native'
import styles from './Styles/EditCharitieStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
import {editCharitie,deleteCharitie} from '../../actions';
import {connect} from 'react-redux';
class EditCharitiesForm extends Component{
    constructor(){
        super()
        this.state={
            name: '', 
            description: '',
            image: '',
            phone_number:'',
            website:''
        }
    }
    componentWillMount(){
        BackHandler.addEventListener('backCedit',()=>{
            Actions.charitieDetail({charitie: this.props.charitie})
            return true;
        })
    }
    firstValue=()=>{
        const{name,description,img_url,phone_number,website}=this.props.charitie
        this.state.name=name
        this.state.description=description
        this.state.image=img_url
        this.state.phone_number=phone_number
        this.state.website=website
    }
    onTryCreateAccount(){
        if(this.props.loading){
            return <Spinner size="large"/>
        }
        return(
            <Button style={[]} onPress={this.onEditDone}>
                Done
            </Button>
        )
    }
    onCreateFail(){
        if(this.props.error)
            return(
                <View>
                    <Text style={{color:'red'}}>
                        {this.props.error}
                    </Text>
                </View>
            )
    }
    onEditDone=()=>{
        name=this.state.name
        description=this.state.description
        img_url=this.state.image
        phone_number=this.state.phone_number
        website=this.state.website
        this.props.editCharitie(this.props.token,this.props.charitie._id,{name,description,img_url,phone_number,website})
    }
    componentWillMount(){
        this.firstValue()
    }
    render(){
        return(
            <View style={styles.mainView}>
            <ScrollView>
                <ImageBackground source={images.backgroundProducts} style={styles.backgroundImageSize}>
                    <View style={styles.bigHeader}>
                        <View style={styles.headerView}>
                            <TouchableOpacity onPress={()=>{Actions.charitieDetail({charitie: this.props.charitie})}} style={styles.headerPosition}>
                                <Image source={images.backPinkArrow} style={styles.imageBack}/>
                                <Text style={styles.cancelStyle}>
                                    Cancel
                                </Text>
                            </TouchableOpacity>
                            <View style={styles.headerPosition}> 
                                <Text style={styles.titleStyle}>
                                    Edit Cause
                                </Text>
                            </View> 
                            <TouchableOpacity onPress={()=>{this.onEditDone()}} style={styles.headerPosition}>
                                <Text style={styles.addStyle}>
                                    Save changes
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.imageViewStyle}>
                            <Image source={{uri: this.state.image}} style={styles.imageStyle}/>
                        </View>
                        <Text style={styles.textInfoEdit}>
                            Edit cause info
                        </Text>
                    </View>
                </ImageBackground>
                <View style={styles.editView}>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Image link
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            value={this.state.image}
                            onChangeText={(image) => this.setState({image})}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Title
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(name) => this.setState({name})}
                            value={this.state.name}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Website  
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(website) => this.setState({website})}
                            value={this.state.website}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Phone Number 
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(phone_number) => this.setState({phone_number})}
                            value={`${this.state.phone_number}`}
                        />
                    </View>
                </View>  
                <View style={styles.descriptionView}>
                    <View style={styles.descriptionTitle}>
                        <Text  style={styles.descriptionTitleText}>
                            Edit cause description
                        </Text>
                    </View>
                    <View style={styles.inputDescriptionView}>          
                        <TextInput
                            style={styles.inputTextDescription}
                            secureTextEntry={false}
                            placeholder='What my product is'
                            autoCorrect={false}
                            value={this.state.description}
                            onChangeText={(description) => this.setState({description})}
                            multiline={true}
                        />
                    </View>
                </View>   
            </ScrollView>
        </View>
        )
    }
}
const mapStateToProps = ({auth,charities}) =>{
    const token=auth
    const{error,loading}=charities
    return {token,error,loading}
}
export default connect(mapStateToProps,{editCharitie,deleteCharitie})(EditCharitiesForm)
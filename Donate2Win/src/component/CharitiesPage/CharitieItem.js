import React, {Component} from 'react';
import {View, Image, Text,TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import styles from './Styles/CharitieItemStyle'
class CharitieItem extends Component {
    onRowPress(){
        Actions.charitieDetail({charitie: this.props.data})
    }
    renderDescription(){
    }
    render(){
        const{name, img_url, description} = this.props.data
        return (
            <TouchableOpacity onPress={()=>{this.onRowPress()}}>
                <View style={styles.mainView}>
                    <Image source={{uri: img_url}} style={styles.imageStyle} />
                    <View style={styles.textView}>
                        <Text style={styles.titleStyle}>
                            {name}
                        </Text>
                        <Text style={styles.descriptionStyle}>
                            {description.substring(0,20)+"..."}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
const mapStateToProps = ({auth}) =>{
    const token=auth
    return {token}
}
export default connect(mapStateToProps,{})(CharitieItem)
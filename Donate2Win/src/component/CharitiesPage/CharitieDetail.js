import React, {Component} from 'react';
import {BackHandler,ScrollView,View,Text,Image,TextInput,FlatList,TouchableOpacity,ImageBackground} from 'react-native'
import {Card, CardSection, Button} from '../common/index';
import {deleteCharitie} from '../../actions';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux'
import styles from './Styles/CharitieDetailStyle'
import images from '../../images'
class CharitieDetail extends Component{
    componentWillMount(){
        BackHandler.addEventListener('backCD',()=>{
            Actions.charitieMain()
            return true;
        })
    }
    render(){
        const {name,description,img_url,_id,phone_number,website}=this.props.charitie
        return(
            <View style={styles.mainView}>
                <ScrollView>
                    <ImageBackground source={images.backgroundProducts} style={styles.backgroundImageSize}>
                        <View style={styles.bigHeader}>
                            <View style={styles.headerView}>
                                <TouchableOpacity onPress={()=>{Actions.charitieMain()}} style={styles.headerPosition}>
                                    <Image source={images.backPinkArrow} style={styles.imageBack}/>
                                    <Text style={styles.cancelStyle}>
                                        Causes
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>{Actions.editCharitie({charitie: this.props.charitie})}} style={styles.headerPosition}>
                                    <Text style={styles.editStyle}>
                                        Edit
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.imageViewStyle}>
                                <Image source={{uri: img_url}} style={styles.imageStyle}/>
                            </View>
                            <Text style={styles.textInfoEdit}>
                                {name}
                            </Text>
                        </View>
                    </ImageBackground>    
                    <View style={styles.descriptionView}>
                        <Text style={styles.textDescription}>
                            {description}
                        </Text>
                        <Text style={styles.textDescription}>
                            {website}
                        </Text>
                        <Text style={styles.textDescription}>
                            {phone_number}
                        </Text>
                    </View>
                    <View style={styles.comandsStyle}>
                        <TouchableOpacity onPress={()=>Actions.editCharitie({charitie: this.props.charitie})}>
                            <Text style={styles.modifyText}>
                                Edit cause
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.barStyle}/>
                        <TouchableOpacity onPress={()=>{this.props.deleteCharitie(this.props.token,_id)}}>
                            <Text style={styles.modifyText}>
                                Delete cause
                            </Text>
                        </TouchableOpacity>                               
                    </View>
                </ScrollView>   
            </View>
        )
    }
}
const mapStateToProps = ({auth}) =>{
    const token=auth
    return {token}
}
export default connect(mapStateToProps,{deleteCharitie})(CharitieDetail)
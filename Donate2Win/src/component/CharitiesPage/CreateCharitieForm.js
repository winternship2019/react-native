import React, {Component} from 'react';
import { Button, Spinner} from '../common/index';
import {connect} from 'react-redux'
import {createCharitie} from '../../actions'
import ImagePicker from 'react-native-image-picker'
import {BackHandler,ScrollView,View,Text,Image,TextInput,FlatList,TouchableOpacity,ImageBackground} from 'react-native'
import styles from './Styles/CreateCharitieStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
class CreateCharitiesForm extends Component{
    constructor(){
        super()
        this.state={
            name: '', 
            description: '',
            image: 'https://i.imgur.com/tP72BMI.png',
            phone_number: '',
            website: ''
        }
    }
    componentWillMount(){
        BackHandler.addEventListener('backCU',()=>{
            Actions.charitieMain()
            return true;
        })
    }
    onCreatePress=()=>{
        const{name,description,image,phone_number,website}=this.state
        this.props.createCharitie({name,description,image,phone_number,website},this.props.token)
    }
    onTryCreateAccount(){
        if(this.props.loading){
            return <Spinner size="large"/>
        }
        return(
            <Button style={[]} onPress={this.onCreatePress}>
                Create
            </Button>
        )
    }
    onCreateFail(){
        if(this.props.error)
            return(
                <View>
                    <Text style={{color:'red'}}>
                        {this.props.error}
                    </Text>
                </View>
            )
    }
    onSelectImage=()=>{
        const options = {
            noData:true
        }
        ImagePicker.launchImageLibrary(options, response => {
            if(response.uri)
                this.setState({image: response})
        });
    }
    render(){
        return(
            <View style={styles.mainView}>
                <ScrollView>
                    <ImageBackground source={images.backgroundProducts} style={styles.backgroundImageSize}>
                        <View style={styles.bigHeader}>
                            <View style={styles.headerView}>
                                <TouchableOpacity onPress={()=>{Actions.charitieMain()}} style={styles.headerPosition}>
                                    <Image source={images.backPinkArrow} style={styles.imageBack}/>
                                    <Text style={styles.cancelStyle}>
                                        Cancel
                                    </Text>
                                </TouchableOpacity>
                                <View style={styles.headerPosition}> 
                                    <Text style={styles.titleStyle}>
                                        New Cause
                                    </Text>
                                </View> 
                                <TouchableOpacity onPress={()=>{this.onCreatePress()}} style={styles.headerPosition}>
                                    <Text style={styles.addStyle}>
                                        Add Cause
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.imageViewStyle}>
                                <Image source={{uri: this.state.image}} style={styles.imageStyle}/>
                            </View>
                            <Text style={styles.textInfoEdit}>
                                Edit cause info
                            </Text>
                        </View>
                    </ImageBackground>
                    <View style={styles.editView}>
                        <View style={styles.inputStyle}>
                            <Text style={styles.textInputStyle}>
                                Image link
                            </Text>           
                            <TextInput
                                style={styles.inputTextStyle}
                                secureTextEntry={false}
                                placeholder=''
                                autoCorrect={false}
                                value={this.state.image}
                                onChangeText={(image) => this.setState({image})}
                            />
                        </View>
                        <View style={styles.inputStyle}>
                            <Text style={styles.textInputStyle}>
                                Title
                            </Text>           
                            <TextInput
                                style={styles.inputTextStyle}
                                secureTextEntry={false}
                                placeholder=''
                                autoCorrect={false}
                                onChangeText={(name) => this.setState({name})}
                                value={this.state.name}
                            />
                        </View>
                        <View style={styles.inputStyle}>
                            <Text style={styles.textInputStyle}>
                                Website
                            </Text>           
                            <TextInput
                                style={styles.inputTextStyle}
                                secureTextEntry={false}
                                placeholder=''
                                autoCorrect={false}
                                onChangeText={(website) => this.setState({website})}
                                value={this.state.website}
                            />
                        </View>
                        <View style={styles.inputStyle}>
                            <Text style={styles.textInputStyle}>
                                Phone number 
                            </Text>           
                            <TextInput
                                style={styles.inputTextStyle}
                                secureTextEntry={false}
                                placeholder=''
                                autoCorrect={false}
                                onChangeText={(phone_number) => this.setState({phone_number})}
                                value={`${this.state.phone_number}`}
                            />
                        </View>
                    </View>  
                    <View style={styles.descriptionView}>
                        <View style={styles.descriptionTitle}>
                            <Text  style={styles.descriptionTitleText}>
                                Edit product description
                            </Text>
                        </View>
                        <View style={styles.inputDescriptionView}>          
                            <TextInput
                                style={styles.inputTextDescription}
                                secureTextEntry={false}
                                placeholder='What my product is'
                                autoCorrect={false}
                                value={this.state.description}
                                onChangeText={(description) => this.setState({description})}
                                multiline={true}
                            />
                        </View>
                    </View>   
                </ScrollView>
            </View>
        )
    }
}
const mapStateToProps=({charities,auth})=>{
    const{error,loading}=charities
    const token=auth
    return {error,loading,token}
}
export default connect(mapStateToProps,{createCharitie})(CreateCharitiesForm)
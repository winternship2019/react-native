import React, {Component} from 'react';
import {Button, Spinner} from '../common/index';
import {connect} from 'react-redux'
import {createProduct,productErrorCreate} from '../../actions'
import {Alert,BackHandler,ScrollView,View,Text,Image,TextInput,FlatList,TouchableOpacity,ImageBackground} from 'react-native'
import styles from './Styles/CreateProductStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
import CreateCharitieItem from './CreateCharitieItem';

class CreateProductForm extends Component{
    constructor(){
        super()
        this.state={
            name:'',
            img_url:'https://i.imgur.com/tP72BMI.png',
            description:'',
            price:'',
        }
    }
    componentWillMount(){
        BackHandler.addEventListener('backCP',()=>{
            Actions.postMain()
            return true;
        })
    }
    onCreatePress=()=>{
        
        const{name,img_url,description,price,total_value}=this.state
        charity=this.props.charitieId
        this.props.createProduct({name,img_url,description,price,charity,total_value},this.props.token)   
        this.onCreateFail()
    }
    onCreateFail(){
        if(this.props.error)
            return(
                Alert.alert(
                    'Save changes',
                    "Error can't save",
                    [
                      {text: 'Ok', onPress: () => this.props.productErrorCreate(false)},
                    ],
                    {cancelable: false},
                  )
            )
    }
    renderRow(data){
        return <CreateCharitieItem data={data.item}/>
    }
    render(){
        return(
            <View style={styles.mainView}>
                <ScrollView>
                    <ImageBackground source={images.backgroundProducts} style={styles.backgroundImageSize}>
                        <View style={styles.bigHeader}>
                            <View style={styles.headerView}>
                                <TouchableOpacity onPress={()=>{Actions.postMain()}} style={styles.headerPosition}>
                                    <Image source={images.backPinkArrow} style={styles.imageBack}/>
                                    <Text style={styles.cancelStyle}>
                                        Cancel
                                    </Text>
                                </TouchableOpacity>
                                <View style={styles.headerPosition}> 
                                    <Text style={styles.titleStyle}>
                                        New product
                                    </Text>
                                </View> 
                                <TouchableOpacity onPress={()=>{this.onCreatePress()}} style={styles.headerPosition}>
                                    <Text style={styles.addStyle}>
                                        Add product{this.onCreateFail()}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.imageViewStyle}>
                                <Image source={{uri: this.state.img_url}} style={styles.imageStyle}/>
                            </View>
                            <Text style={styles.textInfoEdit}>
                                Edit product info
                            </Text>
                        </View>
                    </ImageBackground>
                    <View style={styles.editView}>
                        <View style={styles.inputStyle}>
                            <Text style={styles.textInputStyle}>
                                Image link
                            </Text>           
                            <TextInput
                                style={styles.inputTextStyle}
                                secureTextEntry={false}
                                placeholder=''
                                autoCorrect={false}
                                value={this.state.img_url}
                                onChangeText={(img_url) => this.setState({img_url})}
                            />
                        </View>
                        <View style={styles.inputStyle}>
                            <Text style={styles.textInputStyle}>
                                Title
                            </Text>           
                            <TextInput
                                style={styles.inputTextStyle}
                                secureTextEntry={false}
                                placeholder=''
                                autoCorrect={false}
                                onChangeText={(name) => this.setState({name})}
                                value={this.state.name}
                            />
                        </View>
                        <View style={styles.inputStyle}>
                            <Text style={styles.textInputStyle}>
                                Total value  
                            </Text>           
                            <TextInput
                                style={styles.inputTextStyle}
                                secureTextEntry={false}
                                placeholder=''
                                autoCorrect={false}
                                onChangeText={(price) => this.setState({price})}
                                value={`${this.state.price}`}
                            />
                        </View>
                    </View>  
                    <View style={styles.descriptionView}>
                        <View style={styles.descriptionTitle}>
                            <Text  style={styles.descriptionTitleText}>
                                Edit product description
                            </Text>
                        </View>
                        <View style={styles.inputDescriptionView}>          
                            <TextInput
                                style={styles.inputTextDescription}
                                secureTextEntry={false}
                                placeholder='What my product is'
                                autoCorrect={false}
                                value={this.state.description}
                                onChangeText={(description) => this.setState({description})}
                                multiline={true}
                            />
                        </View>
                    </View>   
                    <View style={styles.charitieView}>
                        <Text style={styles.titleCharitie}>
                            Organization campaign
                        </Text>
                        <View style={styles.charitiePickerView}> 
                            <FlatList
                                    data={this.props.data}
                                    renderItem={this.renderRow}
                                    keyExtractor={data => data._id}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const mapStateToProps=({product,auth,charities})=>{
    const{error,loading,charitieId}=product
    const {data}=charities
    const {token}=auth
    return {error,loading,token,data,charitieId}
}
export default connect(mapStateToProps,{createProduct,productErrorCreate})(CreateProductForm)
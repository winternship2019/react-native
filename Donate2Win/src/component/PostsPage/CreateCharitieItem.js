import React, {Component} from 'react';
import {View, Image, Text,TouchableOpacity} from 'react-native';
import styles from './Styles/CharitieItem'
import {connect} from 'react-redux'
import {idChanged} from '../../actions'
class CreateCharitieItem extends Component {
    componentWillMount(){
        this.onSelectStyle()
        this.props.idChanged('')
    }
    onClick(){
        this.props.idChanged(this.props.data._id)
    }
    onSelectStyle(){
        if(this.props.charitieId==this.props.data._id)
        {
            return styles.mainViewSelected
        }
        return styles.mainView
    }
    render(){
        const{
            name,
            img_url,
            description,
        } = this.props.data
        return (
            <TouchableOpacity onPress={()=>{this.onClick()}}>
                <View style={this.onSelectStyle()}>
                    <Image source={{uri: img_url}} style={styles.imageStyle} />
                    <View style={styles.textView}>
                        <Text style={styles.titleStyle}>
                            {name}
                        </Text>
                        <Text style={styles.descriptionStyle}>
                            {description.substring(0,20)+"..."}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
const mapStateToProps=({product})=>{
    const{charitieId}=product
    return {charitieId}
}
export default connect(mapStateToProps,{idChanged})(CreateCharitieItem)

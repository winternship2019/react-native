import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    mainView:{
        flexDirection: 'column',
        opacity: 100,
        flex:1,
    },
    backgroundImageSize:{
        height: moderateScale(375),
        backgroundColor: '#3F4553',
    },
    background2ImageSize:{
        width:width,
        backgroundColor: 'white'
    },
    bigHeader:{
        position: 'absolute',
        width: width,
        flexDirection: 'column',
    },
    headerView:{
        position: 'absolute',
        height: moderateScale(64),
        flexDirection: 'row',
    },
    headerPosition:{
        paddingTop:moderateScale(5),
        flexDirection: 'row',
        alignSelf: 'center',
    },
    imageBack:{
        marginLeft:moderateScale(8.5)
    },
    backStyle:{
        color:'#FFFFFF',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 20,
        opacity: 100,
        marginLeft: moderateScale(5.5),
        width:moderateScale(77)
    },
    editStyle:{
        color:'#FFFFFF',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        paddingLeft:moderateScale(215),
        
    },
    titleTextStyle:{
        color:'#000000',
        fontSize: 34,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'bold',
        letterSpacing: -0.41,
        lineHeight: 41,
        opacity: 100,
        marginLeft:moderateScale(16),
        marginTop:moderateScale(10),
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    viewDetailsProduct:{
        flexDirection: 'row',
        borderBottomWidth: moderateScale(1),
        borderColor: '#DADADA',
        opacity: 100,
    },
    viewDetails:{
        paddingTop: moderateScale(8),
        paddingLeft: moderateScale(16)
    },
    detailStyle:{
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.1,
        lineHeight: 18,
        opacity: 100,
        marginTop: moderateScale(6),
        marginBottom: moderateScale(6)
    },
    littlePriceStyle:{
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.1,
        lineHeight: 18,
        opacity: 100,
        color:'#000000',
        fontWeight: 'bold',        
    },
    viewPrice:{
        marginTop: moderateScale(8),
        marginLeft: moderateScale(103),
        borderRadius: moderateScale(23),
        height: moderateScale(28),
        width: moderateScale(80),
        opacity:100,
        backgroundColor: '#6CE0D4',
    },
    textPriceStyle:{
        paddingTop:moderateScale(5),
        fontSize: 16,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.32,
        lineHeight: 19,
        opacity: 100,
        fontWeight: 'bold',
    },
    statusView:{
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    campaignStyle:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.09,
        lineHeight: 26,
        opacity: 100,
        marginTop: moderateScale(7.5),
        marginLeft: moderateScale(16),
        marginBottom: moderateScale(9.5),
    },
    barStyle:{
        marginLeft: moderateScale(16),
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    startDateStyle:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.09,
        lineHeight: 26,
        opacity: 100,
        marginTop: moderateScale(7.5),
        marginLeft: moderateScale(16),
        marginBottom: moderateScale(9.5),
    },
    charitieView:{
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        height:moderateScale(62)
    },
    textDescription:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.24,
        lineHeight: 18,
        opacity: 100,
        color:'#8C8C8C',
        marginTop: moderateScale(12),
        marginLeft: moderateScale(16),
        marginBottom: moderateScale(12),
    },
    comandsStyle:{
        backgroundColor: 'white',
        width: width,
        paddingBottom: moderateScale(20)
    },
    modifyText:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.09,
        lineHeight: 26,
        opacity: 100,
        color:'#E34256',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(10),
        marginBottom: moderateScale(10)
    },
})
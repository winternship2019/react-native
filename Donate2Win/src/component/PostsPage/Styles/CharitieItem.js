import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    mainView:{
        flexDirection: 'row',
        opacity: 100,
        flex:1,
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        width:width,
        height:moderateScale(63),
        backgroundColor: 'white',
    },
    mainViewSelected:{
        flexDirection: 'row',
        opacity: 100,
        flex:1,
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        width:width,
        height:moderateScale(63),
        backgroundColor: '#DADADA',
    },
    imageStyle:{
        marginTop: moderateScale(16),
        marginLeft: moderateScale(16),
        height: moderateScale(29),
        width: moderateScale(29)
    },
    textView:{
        flexDirection: 'column',
        marginLeft: moderateScale(20),
        marginTop: moderateScale(7)
    },
    titleStyle:{
        color:'black',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
    },
    descriptionStyle:{
        color: '#8A8A8F',
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
    }

})
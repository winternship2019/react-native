import { moderateScale } from 'react-native-size-matters'
import {StyleSheet} from 'react-native'
export default StyleSheet.create({
    viewProduct:{
        flexDirection: 'row',
        borderBottomWidth: moderateScale(1),
        borderColor: '#DADADA',
        opacity: 100,
        marginLeft: moderateScale(20), 
        height: moderateScale(113),
    },
    viewImage:{
        paddingTop: moderateScale(8),
    },
    imageStyle:{
        height: moderateScale(88),
        width: moderateScale(88)
    },
    viewDetails:{
        paddingTop: moderateScale(8),
        paddingLeft: moderateScale(14)
    },
    nameStyle:{
        color:'#000000',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        width: moderateScale(130),
        height: moderateScale(23)
    },
    detailStyle:{
        fontSize: 13,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.08,
        lineHeight: 18,
        opacity: 100
    },
    littlePriceStyle:{
        fontSize: 13,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.08,
        lineHeight: 18,
        opacity: 100,
        color:'#000000',
        fontWeight: 'bold',
    },
    viewPrice:{
        marginTop: moderateScale(8),
        marginRight: moderateScale(21),
        borderRadius: moderateScale(23),
        height: moderateScale(28),
        width: moderateScale(80),
        opacity:100,
        backgroundColor: '#6CE0D4',
    },
    textPriceStyle:{
        paddingTop:moderateScale(5),
        fontSize: 16,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.32,
        lineHeight: 19,
        opacity: 100,
        fontWeight: 'bold',
    },
    deleteViewStyle:{
        marginLeft:moderateScale(4),
        marginRight:moderateScale(16),
        alignSelf:'center'
    },
    deleteFalse:{
        width: moderateScale(24),
        height: moderateScale(24),
        backgroundColor:'white',
        borderColor:'black',
        borderWidth:moderateScale(0.5),
        borderRadius:moderateScale(12)
    },
    deleteTrue:{
        width: moderateScale(24),
        height: moderateScale(24),
        backgroundColor:'#E34256',
        borderColor:'black',
        borderWidth:moderateScale(0.5),
        borderRadius:moderateScale(12)
    }
    
})
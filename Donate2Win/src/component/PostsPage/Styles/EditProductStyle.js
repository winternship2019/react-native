import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    mainView:{
        flexDirection: 'column',
        opacity: 100,
        flex:1,
    },
    backgroundImageSize:{
        height: moderateScale(275),
        backgroundColor: 'white'
    },
    bigHeader:{
        position: 'absolute',
        width: width,
        flexDirection: 'column',
    },
    headerView:{
        position: 'absolute',
        height: moderateScale(64),
        flexDirection: 'row',
    },
    headerPosition:{
        paddingTop:moderateScale(5),
        width: width/3,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    imageBack:{
        marginLeft:moderateScale(8.5)
    },
    cancelStyle:{
        color:'#3F4553',
        fontSize: 17,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        marginLeft: moderateScale(5.5)
    },
    titleStyle:{
        color:'#000000',
        fontSize: 17,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        fontWeight: 'bold',
        fontStyle: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        marginLeft:moderateScale(25)
    },
    addStyle:{
        color:'#E34256',
        fontSize: moderateScale(17),
        textAlign: 'right',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        height:moderateScale(21),
        marginLeft: moderateScale(10)
    },
    imageViewStyle:{
        alignSelf:'center',
        marginTop: moderateScale(84)
    },
    imageStyle:{
        height:150,
        width:150,
        borderRadius:25,
        backgroundColor: '#FAFAFA'
    },
    textInfoEdit:{
        color:'#000000',
        fontSize: 13,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        fontStyle: 'normal',
        letterSpacing: -0.08,
        lineHeight: 18,
        opacity: 100,
        marginLeft: moderateScale(17),
        marginTop: moderateScale(17)
    },
    editView:{
        backgroundColor: 'white',
        width: width,
        height: moderateScale(160),
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    inputStyle:{
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(10),
        flexDirection: 'row',
        opacity: 100,  
        height:40  
    },
    textInputStyle:{
        color: '#8A8A8F',
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
        marginTop: moderateScale(8),
        flex:1
    },
    inputTextStyle:{
        color: '#8A8A8F',
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
        flex:2,
        borderLeftWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    descriptionView:{
        width: width,
    },
    descriptionTitle:{
        marginTop: moderateScale(20),
        marginLeft: moderateScale(17)
    },
    descriptionTitleText:{
        color: '#8A8A8F',
        fontSize: 13,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.08,
        lineHeight: 18,
        opacity: 100,
        fontStyle: 'normal',
    },
    inputDescriptionView:{
        backgroundColor: 'white',
        width: width,
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        marginTop: moderateScale(21)
    },
    inputTextDescription:{
        color: '#8A8A8F',
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.24,
        lineHeight: 20,
        opacity: 100,
        fontStyle: 'normal',
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
        marginLeft: moderateScale(21)
    },
    charitieView:{
        width: width,
    },
    titleCharitie:{
        color: '#8A8A8F',
        fontSize: 13,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        letterSpacing: -0.08,
        lineHeight: 18,
        opacity: 100,
        fontStyle: 'normal',
        marginTop: moderateScale(20),
        marginLeft: moderateScale(17)
    },
    charitiePickerView:{
        width: width,
        marginTop: moderateScale(9),
    }
})
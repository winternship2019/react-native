import React, {Component} from 'react';
import {View, Image, Text,TouchableWithoutFeedback,TouchableOpacity} from 'react-native';
import { Button} from '../common/index';
import {editProductDelete,setProductDelete} from '../../actions';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import styles from './Styles/ProductItemStyle'
class ProductItem extends Component {
    constructor(){
        super()
        this.state={
            productsForDelete:[],
            isDelete:false,
            boolStyle:styles.deleteFalse
        }
    }
    onRowPress(){
        if(!this.props.editActive)
            Actions.productDetail({product: this.props.data})
    }
    onDeletePress(){
        if(!this.state.isDelete){
            this.props.deleteProducts.push(this.props.data._id)
            this.setState({isDelete: true, boolStyle: styles.deleteTrue})
        }    
        else{
            for(i=0;i<this.props.deleteProducts.length;i++){
                if(this.props.deleteProducts[i]==this.props.data._id){
                    this.props.deleteProducts[i]=this.props.deleteProducts[this.props.deleteProducts.length-1]
                    this.props.deleteProducts.pop()
                    break
                }
                
            }
            this.setState({isDelete: false, boolStyle: styles.deleteFalse})
        }
        this.props.setProductDelete(this.props.deleteProducts)
    }
    editSelectedOption(){
        if(this.props.editActive)
            return(
                <View style={styles.deleteViewStyle}>
                    <TouchableOpacity onPress={()=> this.onDeletePress()}>
                        <View style={this.state.boolStyle}/>
                    </TouchableOpacity>
                </View>
        )
        if(!this.props.editActive && this.state.isDelete)
            this.setState({isDelete: false, boolStyle: styles.deleteFalse})
        
    }
    getData(endDate){
        currentDay= new Date()
        endingDay= new Date(endDate)
        if(currentDay.getTime()<endingDay.getTime())
        {
            return 'Active'
        }
        else
            return 'Suspended'
    }
    
    render(){
        const{
            name,
            img_url,
            bid_price,
            reserve_amount,
            bid_counter,
            end_date,

        } = this.props.data
        return (
            <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
                <View style={styles.viewProduct}>
                    {this.editSelectedOption()}
                    <View style={styles.viewImage}>
                        <Image style={styles.imageStyle} source={{uri: img_url}}/>
                    </View>
                    <View style={styles.viewDetails}>
                        <Text style={styles.nameStyle}>
                            {name}
                        </Text>
                        <Text style={styles.nameStyle}>
                            {this.getData(end_date)}
                        </Text>
                        <Text style={styles.detailStyle}>
                            {bid_counter} bids
                        </Text>
                        <Text style={styles.detailStyle}>
                            {(((bid_price*bid_counter)/reserve_amount)*100).toFixed(2)+ '% out of'}  
                            <Text style={styles.littlePriceStyle}>
                                { ' '+reserve_amount} lei
                            </Text>
                        </Text>
                    </View>
                    <View style={styles.viewPrice}>
                        <Text style={styles.textPriceStyle}>
                            { bid_price} lei
                        </Text>
                    </View> 
                </View>
            </TouchableWithoutFeedback>
        )
    }
}
const mapStateToProps = ({auth,product}) =>{
    const token=auth
    const {deleteProducts,editActive}=product
    return {token,deleteProducts,editActive}
}
export default connect(mapStateToProps,{setProductDelete,editProductDelete})(ProductItem)
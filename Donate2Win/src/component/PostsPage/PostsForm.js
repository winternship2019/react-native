import React, {Component} from 'react';
import ProductItem from './ProductItem'
import {Alert,BackHandler,Image,Text,TouchableOpacity,ImageBackground,View,TextInput , FlatList} from 'react-native';
import {
    editProductDelete,
    inactiveProductData,
    productsData,
    activeProductData,
    charitiesData,
    productSearched,
    usersData,
    setProductDelete,
    deleteProduct
} from '../../actions';
import {connect} from 'react-redux';
import styles from './Styles/PostFormStyle'
import images from '../../images'
import { BottomBar } from '../common';
import { Actions } from 'react-native-router-flux';

class PostsForm extends Component{
    constructor(){
        super()
        this.state={
            search:'',
            allProductsButtonStyle:styles.activeButtonCat,
            inactiveProductsButtonStyle:styles.inactiveButtonCat,
            activeProductsButtonStyle:styles.inactiveButtonCat,
            allProductsText:styles.activeText,
            inactiveProductsText:styles.inactiveText,
            activeProductsText: styles.inactiveText
        }
    }
    activeSelected(){
        this.props.productSearched(this.props.activeProduct)
        this.setState({
            allProductsButtonStyle:styles.inactiveButtonCat,
            inactiveProductsButtonStyle:styles.inactiveButtonCat,
            activeProductsButtonStyle:styles.activeButtonCat,
            allProductsText:styles.inactiveText,
            inactiveProductsText:styles.inactiveText,
            activeProductsText: styles.activeText
        })
    }
    inactiveSelected(){
        this.props.productSearched(this.props.inactiveProduct)
        this.setState({
            allProductsButtonStyle:styles.inactiveButtonCat,
            inactiveProductsButtonStyle:styles.activeButtonCat,
            activeProductsButtonStyle:styles.inactiveButtonCat,
            allProductsText:styles.inactiveText,
            inactiveProductsText:styles.activeText,
            activeProductsText: styles.inactiveText
        })
    }
    allProductSelected(){
        this.props.productSearched(this.props.data)
        this.setState({
            allProductsButtonStyle:styles.activeButtonCat,
            inactiveProductsButtonStyle:styles.inactiveButtonCat,
            activeProductsButtonStyle:styles.inactiveButtonCat,
            allProductsText:styles.activeText,
            inactiveProductsText:styles.inactiveText,
            activeProductsText: styles.inactiveText
        })
    }
    componentDidMount(){
        this.props.productsData()
        this.props.charitiesData()
        this.props.usersData(this.props.token)
        this.props.activeProductData()
        this.props.inactiveProductData()
        this.props.setProductDelete([])
        BackHandler.addEventListener('backPF',()=>{
            Actions.postMain()
            return true;
        });
    }
    handleBackPress = () => {
        
      }
    renderRow(data){
        return <ProductItem data={data.item} />
    }
    searchProduct(search){
        const searched=[]
        this.setState({search: search})

        for(i=0;i<this.props.data.length;i++){
            if(this.props.data[i].name.includes(search)){
                searched.push(this.props.data[i])
            }
        }
        this.props.productSearched(searched)
        
    }
    renderBottomBar(){
        if(this.props.editActive){
           return(
               <View style={styles.deleteBottomView}>
                   <TouchableOpacity onPress={()=> this.props.editProductDelete(false)}>
                        <Text style={styles.cancelDeleteStyle}>
                            Cancel
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{
                        Alert.alert(
                            'Are you sure ?',
                            'Delete all selected products',
                            [
                              {text: 'Cancel', onPress: () => this.props.editProductDelete(false)},
                              {text: 'Yes', onPress: () => this.deleteSelectedItems()},
                            ],
                            {cancelable: false},
                          )
                    }}>
                        <Text style={styles.deleteStyle}>
                            Delete
                        </Text>
                    </TouchableOpacity>
               </View>
           )
        }
        else{
            return <BottomBar user={false} account={false} charitie={false} product={true}/>
        }
    }
    deleteSelectedItems(){
        for(i=0;i<this.props.deleteProducts.length;i++){
            this.props.deleteProduct(this.props.token,this.props.deleteProducts[i])
        }
        this.props.editProductDelete(false)
    }
    render(){
        return(
            <View style={styles.mainView}>
                <View style={styles.headerPart}>
                    <View style={styles.headerView}>
                        <TouchableOpacity onPress={()=> {
                            this.props.editProductDelete(!this.props.editActive)
                            this.props.setProductDelete([])
                        }}>
                            <Text style={styles.editText}>
                                Edit
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{
                            if(!this.props.editActive)
                                Actions.createProduct()
                        }}>
                            <Image source={images.addProductIcon} style={styles.addText}/>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.allProdText}>
                        All products
                    </Text>
                    <View style={styles.categoriesView}>
                        <TouchableOpacity style={this.state.allProductsButtonStyle} onPress={()=>this.allProductSelected()}>
                            <Text style={this.state.allProductsText}>
                                All
                            </Text>
                        </TouchableOpacity>
                        <View style={{borderWidth:1,borderColor: '#3F4553',borderWidth: 0.5}}/>
                        <TouchableOpacity style={this.state.activeProductsButtonStyle} onPress={()=>this.activeSelected()}>
                            <Text style={this.state.activeProductsText}>
                                Active
                            </Text>
                        </TouchableOpacity>
                        <View style={{borderWidth:1,borderColor: '#3F4553',borderWidth: 0.5}}/>
                        <TouchableOpacity style={this.state.inactiveProductsButtonStyle} onPress={()=>this.inactiveSelected()}>
                            <Text style={this.state.inactiveProductsText}>
                                Suspended
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.searchBarView}>
                        <Image source={images.searchIcon} style={styles.imageSearch}/>
                        <TextInput
                            style={styles.textSearch}
                            secureTextEntry={false}
                            placeholder='Search'
                            autoCorrect={false}
                            value={this.state.search}
                            onChangeText={(search) => {
                                this.searchProduct(search)
                            }}
                            maxLength={30}
                        />
                    </View>
                </View>
                <View style={styles.productsView}>
                    <ImageBackground  
                        source={images.backgroundProducts} 
                        style={styles.backgroundImageSize}
                    >
                        <FlatList
                            data={this.props.dataCopy}
                            renderItem={this.renderRow}
                            keyExtractor={data => data._id}
                        />
                    </ImageBackground>
                </View>
                {this.renderBottomBar()}       
            </View>
        )
    }
}
const mapStateToProps = ({product,auth}) =>{
    const {data,dataCopy,activeProduct,inactiveProduct,editActive,deleteProducts}=product
    const token=auth
    return {data,dataCopy,token,activeProduct,inactiveProduct,editActive,deleteProducts}
}
export default connect(mapStateToProps,{deleteProduct,setProductDelete,editProductDelete,inactiveProductData,activeProductData,productsData,usersData,productSearched,charitiesData})(PostsForm)
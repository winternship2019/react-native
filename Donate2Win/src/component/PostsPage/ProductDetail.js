import React, {Component} from 'react';
import {Alert,BackHandler,ScrollView,View,Text,Image,TouchableOpacity,ImageBackground} from 'react-native'
import {deleteProduct} from '../../actions';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux'
import styles from './Styles/ProductDetailStyle'
import images from '../../images'
import CreateCharitieItem from './CreateCharitieItem';
class ProductDetail extends Component{
    getData(endDate){
        currentDay= new Date()
        endingDay= new Date(endDate)
        if(currentDay.getTime()<endingDay.getTime())
        {
            return 'Active'
        }
        else
            return 'Suspended'
    }
    componentWillMount(){
        BackHandler.addEventListener('backPD',()=>{
            Actions.postMain()
            return true;
        })
    }
    render(){
        const{
            name,
            img_url,
            description,
            _id,
            charity,
            create_date,
            end_date,
            bid_counter,
            reserve_amount,
            bid_price,
        } = this.props.product
        return(
            <View style={styles.mainView}>
                <ScrollView>
                    <ImageBackground source={{uri: img_url}} style={styles.backgroundImageSize}>
                        <View style={styles.bigHeader}>
                            <View style={styles.headerView}>
                                <TouchableOpacity onPress={()=>{Actions.postMain()}} style={styles.headerPosition}>
                                    <Image source={images.backWhiteArrow} style={styles.imageBack}/>
                                    <Text style={styles.backStyle}>
                                        Products
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>Actions.editProduct({product: this.props.product})} style={styles.headerPosition}>
                                    <Text style={styles.editStyle}>
                                        Edit
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ImageBackground>
                    <View>
                        <ImageBackground source={images.backgroundProducts} style={styles.background2ImageSize}>
                            <Text style={styles.titleTextStyle}>
                                {name}
                            </Text>
                            <View style={styles.viewDetailsProduct}>
                                <View style={styles.viewDetails}>
                                    <Text style={styles.detailStyle}>
                                        Total value 
                                        <Text style={styles.littlePriceStyle}>
                                            {' '+reserve_amount} lei
                                        </Text>
                                    </Text>
                                    <Text style={styles.detailStyle}>
                                        {bid_counter} donations {'('+(((bid_price*bid_counter)/reserve_amount)*100).toFixed(2)+'%)'}
                                    </Text>
                                    <Text style={styles.detailStyle}>
                                        {bid_counter*bid_price} lei donated
                                    </Text>
                                </View>
                                <View style={styles.viewPrice}>
                                    <Text style={styles.textPriceStyle}>
                                        { bid_price} lei
                                    </Text>
                                </View> 
                            </View>
                            <View style={styles.statusView}>
                                <Text style={styles.campaignStyle}>
                                    {'Status: '}
                                    <Text>
                                        {this.getData(end_date)+' campaign'}
                                    </Text>
                                </Text>
                                <View style={styles.barStyle}/>
                                <Text style={styles.startDateStyle}>
                                    Added in {create_date.substring(0,10)}
                                </Text>
                            </View>
                            <View style={styles.charitieView}>
                                <CreateCharitieItem data={charity}/>
                            </View>
                            <View>
                                <Text style={styles.textDescription}>
                                    {description}
                                </Text>
                            </View>
                        </ImageBackground>
                    </View>     
                    <View style={styles.comandsStyle}>
                        <TouchableOpacity onPress={()=>Actions.editProduct({product: this.props.product})}>
                            <Text style={styles.modifyText}>
                                Edit product
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.barStyle}/>
                        <TouchableOpacity onPress={()=>{
                            Alert.alert(
                                'Are you sure ?',
                                'Delete this product',
                                [
                                  {text: 'Cancel'},
                                  {text: 'Yes', onPress: () => this.props.deleteProduct(this.props.token,_id)},
                                ],
                                {cancelable: false},
                            )}}>
                            <Text style={styles.modifyText}>
                                Delete product
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.barStyle}/>                                
                    </View>
                </ScrollView>   
            </View>
        )
    }
}
const mapStateToProps = ({auth}) =>{
    const token=auth
    return {token}
}
export default connect(mapStateToProps,{deleteProduct})(ProductDetail)

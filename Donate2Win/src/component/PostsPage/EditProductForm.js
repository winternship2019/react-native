import React, {Component} from 'react';
import {deleteProduct,editProduct,idChanged,productErrorEdit} from '../../actions';
import {connect} from 'react-redux';
import {Button, Spinner} from '../common/index';
import {Alert,BackHandler,ScrollView,View,Text,Image,TextInput,FlatList,TouchableOpacity,ImageBackground} from 'react-native'
import styles from './Styles/EditProductStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
import CreateCharitieItem from './CreateCharitieItem';

class EditProductForm extends Component{
    
    constructor(){
        super()
        this.state={
            name:'',
            img_url:'',
            description:'',
            price:'',
            charitie:''
        }
    }
    componentWillMount(){
        const{name,img_url,price,description,charity}=this.props.product
        this.state.name=name
        this.state.price=price
        this.state.img_url=img_url
        this.state.description=description
        this.state.charitie=charity
        this.props.idChanged(this.props.product.charity._id)
        BackHandler.addEventListener('backPE',()=>{
            Actions.productDetail({product: this.props.product})
            return true;
        })
    }

    onEditPress=()=>{
        const{name,img_url,description,price,charity}=this.state
        this.props.editProduct({name,img_url,description,price,charity},this.props.token,this.props.product._id)
    }
    onEditFail(){
        if(this.props.error)
            return(
                Alert.alert(
                    'Save changes',
                    "Error can't save",
                    [
                      {text: 'Ok', onPress: () => this.props.productErrorEdit(false)},
                    ],
                    {cancelable: false},
                )
            )
    }

    renderRow(data){
        return <CreateCharitieItem data={data.item}/>
    }
    render(){
        return(
            <View style={styles.mainView}>
            <ScrollView>
                <ImageBackground source={images.backgroundProducts} style={styles.backgroundImageSize}>
                    <View style={styles.bigHeader}>
                        <View style={styles.headerView}>
                            <TouchableOpacity onPress={()=>{Actions.productDetail({product: this.props.product})}} style={styles.headerPosition}>
                                <Image source={images.backPinkArrow} style={styles.imageBack}/>
                                <Text style={styles.cancelStyle}>
                                    Cancel
                                </Text>
                            </TouchableOpacity>
                            <View style={styles.headerPosition}> 
                                <Text style={styles.titleStyle}>
                                    Edit Product
                                </Text>
                            </View> 
                            <TouchableOpacity 
                                onPress={()=>{
                                    this.onEditPress()
                                    this.onEditFail()
                                }} 
                                style={styles.headerPosition}
                            >
                                <Text style={styles.addStyle}>
                                    Save changes
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.imageViewStyle}>
                            <Image source={{uri: this.state.img_url}} style={styles.imageStyle}/>
                        </View>
                        <Text style={styles.textInfoEdit}>
                            Edit product info
                        </Text>
                    </View>
                </ImageBackground>
                <View style={styles.editView}>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Image link
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            value={this.state.img_url}
                            onChangeText={(img_url) => this.setState({img_url})}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Title
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(name) => this.setState({name})}
                            value={this.state.name}
                        />
                    </View>
                    <View style={styles.inputStyle}>
                        <Text style={styles.textInputStyle}>
                            Price value  
                        </Text>           
                        <TextInput
                            style={styles.inputTextStyle}
                            secureTextEntry={false}
                            placeholder=''
                            autoCorrect={false}
                            onChangeText={(price) => this.setState({price})}
                            value={`${this.state.price}`}
                        />
                    </View>
                </View>  
                <View style={styles.descriptionView}>
                    <View style={styles.descriptionTitle}>
                        <Text  style={styles.descriptionTitleText}>
                            Edit product description
                        </Text>
                    </View>
                    <View style={styles.inputDescriptionView}>          
                        <TextInput
                            style={styles.inputTextDescription}
                            secureTextEntry={false}
                            placeholder='What my product is'
                            autoCorrect={false}
                            value={this.state.description}
                            onChangeText={(description) => this.setState({description})}
                            multiline={true}
                        />
                    </View>
                </View>   
                <View style={styles.charitieView}>
                    <Text style={styles.titleCharitie}>
                        Organization campaign
                    </Text>
                    <View style={styles.charitiePickerView}> 
                        <FlatList
                                data={this.props.data}
                                renderItem={this.renderRow}
                                keyExtractor={data => data._id}
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
        )
    }
}
const mapStateToProps=({product,auth,charities})=>{
    const{error,loading}=product
    const {data}=charities
    const {token}=auth
    return {error,loading,token,data}
}
export default connect(mapStateToProps,{idChanged,editProduct,deleteProduct,productErrorEdit})(EditProductForm)
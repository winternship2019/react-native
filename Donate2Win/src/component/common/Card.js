import React from 'react'
import {View} from 'react-native'
import styles from './styles/CardStyles'

const Card = (props,style) => {
    return (
        <View style={[style]}>
            {props.children}
        </View>

    )
}
export {Card}
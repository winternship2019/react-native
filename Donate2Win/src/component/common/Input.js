import React from 'react'
import {View,TextInput,Text} from 'react-native'

const Input =({ label,value, onChangeText, secureTextEntry, placeholder,style})=>{
    return(
        <View style={style[0]}>
            <Text style={style[1]}>
                {label}
            </Text>           
           
            <TextInput
                style={style[1]}
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                autoCorrect={false}
                value={value}
                onChangeText={onChangeText}
            />
        </View>
    )
}
export {Input}
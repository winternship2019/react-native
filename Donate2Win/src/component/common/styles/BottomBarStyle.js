import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    pageView:{
        borderTopWidth: 1,
        borderColor:'#DEDEDE',
        position: 'absolute',
        bottom: 0,
        height: moderateScale(49),
        backgroundColor: '#F2F0F0',
        flexDirection: 'row',
    },
    viewPage:{
        width: width/4,
    },
    textPage1:{
        color:'#E34256',
        fontSize: 10,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: 0.16,
        lineHeight: 12,
        opacity: 100,
        paddingTop:moderateScale(5)
    },
    textPage2:{
        fontSize: 10,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: 0.16,
        lineHeight: 12,
        opacity: 100,
        paddingTop:moderateScale(5)
    },
    imagePage:{
        alignSelf: 'center',
        marginTop: moderateScale(6),
    },
    imagePageC:{
        alignSelf: 'center',
        marginTop: moderateScale(9),
    },
    imagePageU:{
        alignSelf: 'center',
        marginTop: moderateScale(10),
    },
})
import React from 'react'
import {Text, TouchableOpacity} from 'react-native'

const Button = ({children,onPress,style}) =>{
    return (
        <TouchableOpacity onPress={onPress} style={style[0]}>
            <Text style={style[1]}>
                {children}
            </Text>
        </TouchableOpacity>
    )
}
export {Button}
import React from 'react'
import {Image,Text,View, TouchableOpacity} from 'react-native'
import styles from './styles/BottomBarStyle'
import images from '../../images'
import { Actions } from 'react-native-router-flux';
const BottomBar = ({user,account,charitie,product}) =>{
    const activeProduct = ()=>{
        if(product)
            return(
                <TouchableOpacity onPress={()=> Actions.postMain()}>
                    <Image source={images.productActivIcon} style={styles.imagePage}/>
                    <Text style={styles.textPage1}>
                        Products
                    </Text>
                </TouchableOpacity>   
            )
        else
            return(
                <TouchableOpacity onPress={()=> Actions.postMain()}>
                    <Image source={images.productIcon} style={styles.imagePage}/>
                    <Text style={styles.textPage2}>
                        Products
                    </Text>
                </TouchableOpacity>  
            )
    }
    const activeCharitie = ()=>{
        if(charitie)
            return(
                <TouchableOpacity onPress={()=> Actions.charitieMain()}>
                    <Image source={images.charityActivIcon} style={styles.imagePageC}/>
                    <Text style={styles.textPage1}>
                        Causes
                    </Text>
                </TouchableOpacity>    
            )
        else
            return(
                <TouchableOpacity onPress={()=> Actions.charitieMain()}>
                    <Image source={images.charityIcon} style={styles.imagePageC}/>
                    <Text style={styles.textPage2}>
                        Causes
                    </Text>
                </TouchableOpacity>    
            )
    }
    const activeAccount = ()=>{
        if(account)
            return(
                <TouchableOpacity onPress={()=> Actions.accountMain()}>
                    <Image source={images.profileActivIcon} style={styles.imagePage}/>
                    <Text style={styles.textPage1}>
                        Account
                    </Text>
                </TouchableOpacity>                
            )
        else
            return(
                <TouchableOpacity onPress={()=> Actions.accountMain()}>
                    <Image source={images.profileIcon} style={styles.imagePage}/>
                    <Text style={styles.textPage2}>
                        Account
                    </Text>
                </TouchableOpacity>  
            )
    }
    const activeUser = ()=>{
        if(user)
            return(
                <TouchableOpacity onPress={()=> Actions.userMain()}>
                    <Image source={images.userActivIcon} style={styles.imagePageU}/>
                    <Text style={styles.textPage1}>
                        Users
                    </Text>
                </TouchableOpacity>
            )
        else
            return(
                <TouchableOpacity onPress={()=> Actions.userMain()}>
                    <Image source={images.userIcon} style={styles.imagePageU}/>
                    <Text style={styles.textPage2}>
                        Users
                    </Text>
                </TouchableOpacity>
            )
    }
    return (
        <View style={styles.pageView}>
            <View style={styles.viewPage}>
                {activeProduct()}
            </View>
            <View style={styles.viewPage}>
                {activeCharitie()}
            </View>
            <View style={styles.viewPage}>
                {activeUser()}
            </View>
            <View style={styles.viewPage}>
                {activeAccount()}
            </View>
        </View>
    )
}
export {BottomBar}
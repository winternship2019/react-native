import React from 'react';
import {View, CheckBox, Text} from 'react-native';
const Bool =({value,onValueChange, style})=>{
    return(
        <View>
            <CheckBox
                value={value}
                onValueChange={onValueChange}
                style={style}
            />
          
        </View>
    )
}
export {Bool}
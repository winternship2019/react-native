import { moderateScale } from 'react-native-size-matters'
import {Dimensions, StyleSheet} from 'react-native'
const {height, width} = Dimensions.get('window')
export default StyleSheet.create({
    mainView:{
        flexDirection: 'column',
        opacity: 100,
        flex:1,
    },
    backgroundImageSize:{
        height: moderateScale(275),
        backgroundColor: 'white'
    },
    bigHeader:{
        position: 'absolute',
        width: width,
        flexDirection: 'column',
    },
    headerView:{
        position: 'absolute',
    },
    headerPosition:{
        paddingTop:moderateScale(25),
        flexDirection: 'row',
        alignSelf: 'center',
    },
    addStyle:{
        color:'#E34256',
        fontSize: moderateScale(17),
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.41,
        lineHeight: 22,
        opacity: 100,
        width:moderateScale(100),
        marginLeft:moderateScale(260)
    },
    imageViewStyle:{
        alignSelf:'center',
        marginTop: moderateScale(76)
    },
    imageStyle:{
        height: moderateScale(86),
        width: moderateScale(86),
        borderRadius:moderateScale(40),
        backgroundColor: '#FAFAFA'
    },
    textInfoEdit:{
        fontSize: 13,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        fontStyle: 'normal',
        letterSpacing: -0.08,
        lineHeight: 18,
        opacity: 100,
        marginLeft: moderateScale(16),
        marginTop: moderateScale(10),
        marginBottom: moderateScale(10)
    },
    titleStyle:{
        width:width,
        height: moderateScale(36),
        color:'#000000',
        fontSize: 30,
        textAlign: 'center',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        fontStyle: 'normal',
        letterSpacing: 0.39,
        lineHeight: 36,
        opacity: 100,
        marginTop:moderateScale(20),
        marginBottom:moderateScale(15)

    },
    generalDetailsView:{
        backgroundColor: 'white',
        width: width,
        flexDirection: 'column',
    },
    infoView:{
        flexDirection:'row',
        marginLeft: moderateScale(16),
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
    textDetail:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        fontStyle: 'normal',
        letterSpacing: -0.09,
        lineHeight: 26,
        opacity: 100,
        width:moderateScale(width/3),
        marginTop:moderateScale(11),
        marginBottom:moderateScale(5),
    },
    textInfoStyle:{
        color: 'black',
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        fontStyle: 'normal',
        letterSpacing: -0.09,
        lineHeight: 26,
        opacity: 100,
        width:moderateScale(width/1.8),
        marginTop:moderateScale(11),
        marginBottom:moderateScale(5),
    },
    buttonsView:{
        marginTop: moderateScale(30),
        width:width,
        height: moderateScale(200),
    },
    buttonBackground:{
        backgroundColor:'white'
    },
    modifyText:{
        fontSize: 15,
        textAlign: 'left',
        fontFamily: 'SF Pro Text',
        fontWeight: 'normal',
        letterSpacing: -0.09,
        lineHeight: 26,
        opacity: 100,
        color:'#E34256',
        marginLeft: moderateScale(16),
        marginTop: moderateScale(10),
        marginBottom: moderateScale(10),
    },
    barStyle:{
        marginLeft: moderateScale(16),
        borderBottomWidth: moderateScale(0.5),
        borderColor: '#BCBBC1',
    },
})
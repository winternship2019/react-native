import React, { Component } from "react";
import { Spinner, Card, Button, CardSection, Input } from "../common/index";
import { editAdmin } from "../../actions";
import ImagePicker from "react-native-image-picker";
import {
  Alert,
  BackHandler,
  TextInput,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import styles from "./Styles/EditStyle";
import images from "../../images";

class EditAccountForm extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      wallet: 0,
      image: "",
      userImage: { fileName: "", type: "", path: "" },
      passwordO: "",
      password: "",
      passwordC: "",
    };
  }
  firstValue = () => {
    const { name, email, img_url, wallet } = this.props.users;
    this.state.name = name;
    this.state.email = email;
    this.state.image = img_url;
    this.state.wallet = wallet;
  };
  componentWillMount() {
    this.firstValue();
    BackHandler.addEventListener("backAE", () => {
      Actions.accountMain();
      return true;
    });
  }
  onEditDone = () => {
    if (this.state.password == this.state.passwordC) {
      name = this.state.name;
      email = this.state.email;
      img_url = this.state.userImage;
      phoneNumber = this.state.phoneNumber;
      wallet = this.state.wallet;
      password = this.state.password;
      passwordO = this.state.passwordO;
      this.props.editAdmin(this.props.token, this.props.users._id, {
        name,
        email,
        img_url,
        wallet,
        password,
        passwordO,
      });
    } else
      return Alert.alert(
        "Try again",
        "Passwords are not matching.",
        [{ text: "Ok" }],
        { cancelable: false }
      );
  };
  onTryEditAccount() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
    return (
      <Button style={[]} onPress={this.onEditDone}>
        Done
      </Button>
    );
  }
  onSelectImage = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response) {
        this.setState({ image: response.uri });
        this.setState({ userImage: response });
      }
    });
  };
  onEditFail() {
    if (this.props.error)
      return (
        <View>
          <Text style={{ color: "red" }}>{this.props.error}</Text>
        </View>
      );
  }
  render() {
    return (
      <View style={styles.mainView}>
        <ScrollView>
          <ImageBackground
            source={images.backgroundProducts}
            style={styles.backgroundImageSize}
          >
            <View style={styles.bigHeader}>
              <View style={styles.headerView}>
                <TouchableOpacity
                  onPress={() => {
                    Actions.accountMain();
                  }}
                  style={styles.headerPosition}
                >
                  <Image
                    source={images.backPinkArrow}
                    style={styles.imageBack}
                  />
                  <Text style={styles.cancelStyle}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.onEditDone}
                  style={styles.headerPosition}
                >
                  <Text style={styles.addStyle}>Save changes</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.imageViewStyle}>
                <TouchableOpacity onPress={this.onSelectImage}>
                  <Image
                    source={{ uri: this.state.image }}
                    style={styles.imageStyle}
                  />
                </TouchableOpacity>
              </View>
              <Text style={styles.imageDescription}>
                Be a human, have a face
              </Text>
              <Text style={styles.textInfoEdit}>Edit account info</Text>
            </View>
          </ImageBackground>
          <View style={styles.editView}>
            <View style={styles.inputStyle}>
              <Text style={styles.textInputStyle}>Display name</Text>
              <TextInput
                style={styles.inputTextStyle}
                secureTextEntry={false}
                placeholder=""
                autoCorrect={false}
                value={this.state.name}
                onChangeText={(name) => this.setState({ name })}
              />
            </View>
            <View style={styles.inputStyle}>
              <Text style={styles.textInputStyle}>Email</Text>
              <TextInput
                style={styles.inputTextStyle}
                secureTextEntry={false}
                placeholder=""
                autoCorrect={false}
                onChangeText={(email) => this.setState({ email })}
                value={this.state.email}
              />
            </View>
            <View style={styles.inputStyle}>
              <Text style={styles.textInputStyle}>Wallet</Text>
              <TextInput
                style={styles.inputTextStyle}
                secureTextEntry={false}
                placeholder=""
                autoCorrect={false}
                onChangeText={(wallet) => this.setState({ wallet })}
                value={`${this.state.wallet}`}
              />
            </View>
            <View style={styles.inputStyle}>
              <Text style={styles.textInputStyle}>Old password</Text>
              <TextInput
                style={styles.inputTextStyle}
                secureTextEntry={true}
                placeholder=""
                autoCorrect={false}
                onChangeText={(passwordO) => this.setState({ passwordO })}
                value={this.state.passwordO}
              />
            </View>
            <View style={styles.inputStyle}>
              <Text style={styles.textInputStyle}>New password</Text>
              <TextInput
                style={styles.inputTextStyle}
                secureTextEntry={true}
                placeholder=""
                autoCorrect={false}
                value={this.state.password}
                onChangeText={(password) => this.setState({ password })}
              />
            </View>
            <View style={styles.inputStyle}>
              <Text style={styles.textInputStyle}>Confirm password</Text>
              <TextInput
                style={styles.inputTextStyle}
                secureTextEntry={true}
                placeholder=""
                autoCorrect={false}
                onChangeText={(passwordC) => this.setState({ passwordC })}
                value={this.state.passwordC}
              />
            </View>
          </View>
          <View style={styles.buttonsView}>
            <TouchableOpacity
              style={styles.buttonBackground}
              onPress={this.onEditDone}
            >
              <Text style={styles.modifyText}>Save changes</Text>
            </TouchableOpacity>
            <View style={styles.barStyle} />
          </View>
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = ({ auth, admin }) => {
  const { token } = auth;
  const { error, loading, users } = admin;
  return { users, token, error, loading };
};
export default connect(mapStateToProps, { editAdmin })(EditAccountForm);

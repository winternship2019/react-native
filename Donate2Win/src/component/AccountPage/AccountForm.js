import React, { Component } from "react";
import {
  Alert,
  BackHandler,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import styles from "./Styles/ProfileStyle";
import images from "../../images";
import { BottomBar, Button } from "../common";
import { resetPassword, adminData, selectWinner } from "../../actions";

class AccountForm extends Component {
  componentWillMount() {
    this.props.adminData(this.props.token, this.props.user._id);
    BackHandler.addEventListener("backAF", () => {
      Actions.accountMain();
      return true;
    });
  }
  onSentFail() {
    if (this.props.error == "Nu merge")
      return (
        <View>
          <Text style={{ color: "red" }}>{this.props.error}</Text>
        </View>
      );
    else
      return (
        <View>
          <Text style={{ color: "green" }}>{this.props.mail}</Text>
        </View>
      );
  }
  onResetPasswordAccount() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
    return (
      <Button
        style={[]}
        onPress={() => this.props.resetPassword(this.props.users.email)}
      >
        Reset Password
      </Button>
    );
  }
  winAlerts() {
    this.props.selectWinner(this.props.token);
  }
  render() {
    return (
      <View style={styles.mainView}>
        <ScrollView>
          <ImageBackground
            source={images.backgroundProducts}
            style={styles.backgroundImageSize}
          >
            <View style={styles.bigHeader}>
              <View style={styles.headerView}>
                <TouchableOpacity
                  onPress={() => Actions.editAccount()}
                  style={styles.headerPosition}
                >
                  <Text style={styles.addStyle}>Edit account</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.imageViewStyle}>
                <Image
                  source={{ uri: this.props.users.img_url }}
                  style={styles.imageStyle}
                />
              </View>
              <Text style={styles.titleStyle}>Admin account</Text>
              <Text style={styles.textInfoEdit}>General info</Text>
            </View>
          </ImageBackground>
          <View style={styles.generalDetailsView}>
            <View style={styles.infoView}>
              <Text style={styles.textDetail}>Products</Text>
              <Text style={styles.textInfoStyle}>
                {this.props.dataProducts.length}
              </Text>
            </View>
            <View style={styles.infoView}>
              <Text style={styles.textDetail}>Causes</Text>
              <Text style={styles.textInfoStyle}>
                {this.props.dataCharities.length}
              </Text>
            </View>
            <View style={styles.infoView}>
              <Text style={styles.textDetail}>Users</Text>
              <Text style={styles.textInfoStyle}>
                {this.props.dataUsers.length}
              </Text>
            </View>
          </View>
          <View>
            <Text style={styles.textInfoEdit}>Account info</Text>
            <View style={styles.generalDetailsView}>
              <View style={styles.infoView}>
                <Text style={styles.textDetail}>Display name</Text>
                <Text style={styles.textInfoStyle}>
                  {this.props.users.name}
                </Text>
              </View>
              <View style={styles.infoView}>
                <Text style={styles.textDetail}>Email</Text>
                <Text style={styles.textInfoStyle}>
                  {this.props.users.email}
                </Text>
              </View>
              <View style={styles.infoView}>
                <Text style={styles.textDetail}>Wallet</Text>
                <Text style={styles.textInfoStyle}>
                  {this.props.users.wallet}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.buttonsView}>
            <TouchableOpacity
              style={styles.buttonBackground}
              onPress={() => Actions.editAccount()}
            >
              <Text style={styles.modifyText}>Edit account</Text>
            </TouchableOpacity>
            <View style={styles.barStyle} />
            <TouchableOpacity
              style={styles.buttonBackground}
              onPress={() => this.winAlerts()}
            >
              <Text style={styles.modifyText}>Select winners</Text>
            </TouchableOpacity>
            <View style={styles.barStyle} />
            <TouchableOpacity
              style={styles.buttonBackground}
              onPress={() => Actions.auth()}
            >
              <Text style={styles.modifyText}>Log-out</Text>
            </TouchableOpacity>
            <View style={styles.barStyle} />
          </View>
        </ScrollView>
        <BottomBar
          user={false}
          account={true}
          charitie={false}
          product={false}
        />
      </View>
    );
  }
}
const mapStateToProps = ({ auth, admin, account, product, charities }) => {
  const dataCharities = charities.data;
  const dataUsers = account.data;
  const dataProducts = product.data;
  const { user, token } = auth;
  const { error, loading, mail, users } = admin;
  return {
    user,
    token,
    loading,
    error,
    mail,
    users,
    dataCharities,
    dataUsers,
    dataProducts,
  };
};
export default connect(mapStateToProps, {
  resetPassword,
  adminData,
  selectWinner,
})(AccountForm);
